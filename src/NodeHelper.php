<?php

namespace Drupal\druhels;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\node\NodeViewBuilder;

class NodeHelper {

  /**
   * Return TRUE if current page is a node page.
   */
  public static function isNodePage(string $node_type = NULL): bool {
    static $current_node_type;

    if ($current_node_type === NULL) {
      if (DrupalHelper::getCurrentRouteName() == 'entity.node.canonical') {
        $current_node_type = self::getCurrentNode()->bundle();
      }
      else {
        $current_node_type = FALSE;
      }
    }

    return $node_type ? ($current_node_type == $node_type) : (bool)$current_node_type;
  }

  /**
   * Return current node id in node page.
   */
  public static function getCurrentNodeId(RouteMatchInterface $route_match = NULL): ?int {
    if (!$route_match) {
      $route_match = \Drupal::routeMatch();
    }
    $node_id = $route_match->getRawParameter('node');
    return $node_id ? (int)$node_id : NULL;
  }

  /**
   * Return current node in node page.
   */
  public static function getCurrentNode(RouteMatchInterface $route_match = NULL): ?NodeInterface {
    $node = NULL;

    if ($nid = self::getCurrentNodeId($route_match)) {
      $node = Node::load($nid);
    }

    return $node;
  }

  /**
   * Return node build array.
   *
   * Example:
   * <code>
   * $node_build = NodeHelper::view(123);
   * // ['#theme' => 'node', ...]
   * </code>
   *
   * @param NodeInterface|integer $node Node object or nid.
   * @param string $view_mode View mode
   */
  public static function view($node, string $view_mode = 'full'): array {
    if (is_numeric($node)) {
      /** @noinspection All */
      $node = Node::load($node);
    }

    if ($node) {
      $node_view_builder = \Drupal::entityTypeManager()->getViewBuilder('node'); /** @var NodeViewBuilder $node_view_builder */
      return $node_view_builder->view($node, $view_mode);
    }

    return [];
  }

  /**
   * Return nodes build array.
   *
   * Example:
   * <code>
   * $nodes_build = NodeHelper::view([1, 2]);
   * // [
   * //   0 => ['#theme' => 'node', ...]
   * //   1 => ['#theme' => 'node', ...]
   * // ]
   * </code>
   */
  public static function viewMultiple(array $nodes, string $view_mode = 'full'): array {
    return array_map(function ($node) use ($view_mode) {
      return self::view($node, $view_mode);
    }, $nodes);
  }

  /**
   * Return node storage.
   */
  public static function getNodeStorage(): NodeStorageInterface {
    /** @noinspection All */
    return \Drupal::entityTypeManager()->getStorage('node');
  }

  /**
   * Return node url object.
   *
   * Example:
   * <code>
   * $url_string = NodeHelper::getTermUrl(123)->toString();
   * // '/node/123'
   * </code>
   */
  public static function getNodeUrl(int $nid, array $options = []): Url {
    return Url::fromRoute('entity.node.canonical', ['node' => $nid], $options);
  }

}
