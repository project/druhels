<?php

namespace Drupal\druhels;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;

class DrupalHelper {

  private static array $timers = [];

  /**
   * Return current page title.
   *
   * @return string|array String or render-array
   */
  public static function getCurrentPageTitle(Request $request = NULL) {
    if (!$request) {
      $request = \Drupal::request();
    }
    $route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);
    $title_resolver = \Drupal::service('title_resolver'); /** @var TitleResolverInterface $title_resolver */
    $page_title = $title_resolver->getTitle($request, $route);

    if (is_array($page_title) && isset($page_title['#markup'])) {
      $page_title = $page_title['#markup'];
    }

    return $page_title;
  }

  /**
   * Set page title.
   *
   * @param string $title Page title
   * @param boolean $unset_route_title_callback Set TRUE if must unset route title callback
   */
  public static function setCurrentPageTitle(string $title, bool $unset_route_title_callback = FALSE, RouteMatchInterface  $route_match = NULL): void {
    if (!$route_match) {
      $route_match = \Drupal::routeMatch();
    }
    $current_route = $route_match->getRouteObject();
    $current_route->setDefault('_title', $title);

    if ($unset_route_title_callback) {
      $current_route->setDefault('_title_callback', NULL);
    }
  }

  /**
   * Return current system path without trailing slashes.
   */
  public static function getCurrentSystemPath(): string {
    $current_system_path = \Drupal::service('path.current')->getPath();
    return ltrim($current_system_path, '/');
  }

  /**
   * Return current path alias without trailing slashes.
   */
  public static function getCurrentPathAlias(): string {
    $current_system_path = self::getCurrentSystemPath();
    $current_path_alias = \Drupal::service('path_alias.manager')->getAliasByPath('/' . $current_system_path);
    return ltrim($current_path_alias, '/');
  }

  /**
   * Return current route name.
   */
  public static function getCurrentRouteName(RouteMatchInterface $route_match = NULL): ?string {
    static $cache = new \SplObjectStorage();

    $request = \Drupal::request();

    if (!isset($cache[$request])) {
      $cache[$request] = \Drupal::routeMatch()->getRouteName();
    }

    return $cache[$request];
  }

  /**
   * Return TRUE if current route is admin route.
   */
  public static function isAdminRoute(bool $reset_cache = FALSE): bool {
    static $cache = NULL;

    if ($reset_cache) {
      $cache = NULL;
    }
    if ($cache === NULL) {
      $cache = \Drupal::service('router.admin_context')->isAdminRoute();
    }

    return $cache;
  }

  /**
   * Return TRUE if current page is front.
   */
  public static function isFrontPage(): bool {
    static $cache = new \SplObjectStorage();

    $request = \Drupal::request();

    if (!isset($cache[$request])) {
      $cache[$request] = \Drupal::service('path.matcher')->isFrontPage();
    }

    return $cache[$request];
  }

  /**
   * Return TRUE if current page is Views page.
   *
   * @TODO Add test
   */
  public static function isViewsPage(string $views_name, string $display_name = NULL, RouteMatchInterface $route_match = NULL): bool {
    static $cache = [];

    $cache_key = $views_name . ':' . $display_name;

    if (!isset($cache[$cache_key])) {
      $current_route_name = self::getCurrentRouteName($route_match);

      if ($display_name) {
        $cache[$cache_key] = ($current_route_name == "view.$views_name.$display_name");
      }
      else {
        $cache[$cache_key] = str_starts_with($current_route_name, "view.$views_name.");
      }
    }

    return $cache[$cache_key];
  }

  /**
   * Start timer.
   *
   * Example:
   * <code>
   * DrupalHelper::timerStart();
   * sleep(1);
   * DrupalHelper::timerStop();
   * </code>
   */
  public static function timerStart(string $name = 'druhels'): void {
    Timer::start($name);
    self::$timers[$name] = memory_get_usage();
  }

  /**
   * Stop timer and show result.
   *
   * Example:
   * <code>
   * DrupalHelper::timerStart();
   * sleep(1);
   * DrupalHelper::timerStop();
   * </code>
   */
  public static function timerStop(string $name = 'druhels', bool $show_result = TRUE): void {
    if ($show_result) {
      $duration = Timer::read($name);
      $memory = memory_get_usage() - self::$timers[$name];
      \Drupal::messenger()->addMessage($duration . ' ms (' . number_format($duration / 1000, 5) . ' sec) / ' . format_size($memory));
    }

    Timer::stop($name);
  }

  /**
   * Return default langcode (language id).
   */
  public static function getDefaultLangcode(): string {
    static $default_language_id;

    if ($default_language_id === NULL) {
      $default_language_id = \Drupal::languageManager()->getDefaultLanguage()->getId();
    }

    return $default_language_id;
  }

  /**
   * Return current langcode (language id).
   */
  public static function getCurrentLangcode(): string {
    static $current_language_id;

    if ($current_language_id === NULL) {
      $current_language_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    return $current_language_id;
  }

  /**
   * Return site mail address.
   */
  public static function getSiteMail(): string {
    return \Drupal::config('system.site')->get('mail');
  }

  /**
   * Send mail without need implements hook_mail().
   */
  public static function sendMail(string $to, string $subject, $message, string $key = 'druhels', string $langcode = NULL, array $params = []) {
    if (!$langcode) {
      $langcode = self::getDefaultLangcode();
    }

    $mail_service = \Drupal::service('plugin.manager.mail'); /* @var MailManagerInterface $mail_service */

    $params['subject'] = $subject;
    $params['body'] = $message;

    return $mail_service->mail('druhels', $key, $to, $langcode, $params);
  }

  /**
   * Return breakpoint media query.
   *
   * Example:
   * <code>
   * $wide_media_query = DrupalHelper::getBreakpointMediaQuery('seven', 'seven.wide');
   * // 'screen and (min-width: 40em)'
   * </code>
   */
  public static function getBreakpointMediaQuery(string $group, string $breakpoint_name): string {
    return \Drupal::service('breakpoint.manager')->getBreakpointsByGroup($group)[$breakpoint_name]->getMediaQuery();
  }

  /**
   * Render without change input data.
   *
   * Example:
   * <code>
   * $output = DrupalHelper::render(['#markup' => 'Hello']);
   * // 'Hello'
   * </code>
   *
   * @param array|mixed $build Render-array ot string
   */
  public static function render($build): string {
    return \Drupal::service('renderer')->render($build);
  }

  /**
   * Remove status message.
   *
   * Example:
   * <code>
   * \Drupal::messanger()->addError('Random error message');
   * DrupalHelper::removeStatusMessage('error', function ($message): bool {
   *   return (string)$message == 'Random error message';
   * });
   * </code>
   */
  public static function removeStatusMessage(string $type, callable $callback): bool {
    $messanger = \Drupal::messenger();
    $messages_by_type = $messanger->messagesByType($type);
    $message_deleted = FALSE;

    foreach ($messages_by_type as $message_key => $message) {
      if ($callback($message)) {
        unset($messages_by_type[$message_key]);
        $message_deleted = TRUE;
      }
    }

    if ($message_deleted) {
      $messanger->deleteByType($type);

      foreach ($messages_by_type as $message) {
        $messanger->addMessage($message, $type);
      }

      return TRUE;
    }

    return FALSE;
  }

}
