<?php

namespace Drupal\druhels;

use DateTimeInterface as PhpDateTimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;

class DateHelper {

  /**
   * Return php DateTime object.
   *
   * Example:
   * <code>
   * $formatted_date = DateHelper::createPhpDateTime('2010-01-01')->format('d.m.Y');
   * // '01.01.2010'
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date
   */
  public static function createPhpDateTime($date): ?PhpDateTimeInterface {
    $datetime = NULL;

    if (is_int($date)) {
      $datetime = new \DateTime("@$date");
    }
    elseif (is_string($date)) {
      $datetime = new \DateTime($date);
    }
    elseif ($date instanceof DrupalDateTime) {
      $datetime = $date->getPhpDateTime();
    }
    elseif ($date instanceof PhpDateTimeInterface) {
      $datetime = $date;
    }

    return $datetime;
  }

  /**
   * Return days count between two dates.
   *
   * Example:
   * <code>
   * $days = DateHelper::getDaysBetweenDates('2010-01-01', '2010-01-07');
   * // 6
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date_start
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date_end
   */
  public static function getDaysBetweenDates($date_start, $date_end): ?int {
    $date_start = self::createPhpDateTime($date_start);
    $date_end = self::createPhpDateTime($date_end);
    return ($date_start && $date_end) ? (int)$date_end->diff($date_start)->format('%a') : NULL;
  }

  /**
   * Return timestamp by date.
   *
   * Example:
   * <code>
   * $timestamp = DateHelper::getDateTimestamp('2010-01-01 UTC');
   * // 1262304000
   * </code>
   *
   * @param int|string|DrupalDateTime|PhpDateTimeInterface $date
   */
  public static function getDateTimestamp($date): ?int {
    if (is_int($date)) {
      return $date;
    }
    if (is_string($date)) {
      return strtotime($date);
    }
    if ($date instanceof DrupalDateTime || $date instanceof PhpDateTimeInterface) {
      return $date->getTimestamp();
    }
    return NULL;
  }

  /**
   * Return TRUE if $date_to_check is between $date_start and $date_end.
   *
   * Example:
   * <code>
   * $result = DateHelper::checkDateInDaterange('2010-06-01', '2010-01-01', '2010-12-31');
   * // TRUE
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date_to_check
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date_start
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date_end
   */
  public static function checkDateInDaterange($date_to_check, $date_start, $date_end): bool {
    $date_to_check = self::getDateTimestamp($date_to_check);
    $date_start = self::getDateTimestamp($date_start);
    $date_end = self::getDateTimestamp($date_end);

    if ($date_start === NULL) {
      $date_start = PHP_INT_MIN;
    }
    if ($date_end === NULL) {
      $date_end = PHP_INT_MAX;
    }

    return ($date_to_check >= $date_start && $date_to_check <= $date_end);
  }

  /**
   * Return date season number.
   *
   * Example:
   * <code>
   * DateHelper::getDateSeason('2010-01-10'); // 0
   * DateHelper::getDateSeason('2010-12-11'); // 4
   * DateHelper::getDateSeason('2010-03-13'); // 1
   * DateHelper::getDateSeason('2010-06-14'); // 2
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date
   *
   * @return int 0 - winter at the beginning of the year, 1 - spring, 2 - summer, 3 - autumn, 4 - winter.
   */
  public static function getDateSeason($date): int {
    $date_month = self::createPhpDateTime($date)->format('n');

    // Winter at beginning of year
    if ($date_month >= 1 && $date_month <= 2) {
      return 0;
    }
    // Spring
    if ($date_month >= 3 && $date_month <= 5) {
      return 1;
    }
    // Summer
    if ($date_month >= 6 && $date_month <= 8) {
      return 2;
    }
    // Autumn
    if ($date_month >= 9 && $date_month <= 11) {
      return 3;
    }
    // Winter at end of year
    else {
      return 4;
    }
  }

  /**
   * Return date season start.
   *
   * Example:
   * <code>
   * DateHelper::getDateSeason('2010-04-12'); // 2010-03-01
   * DateHelper::getDateSeason('2010-01-10'); // 2009-12-01
   * DateHelper::getDateSeason('2010-12-30'); // 2010-12-01
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date
   */
  public static function getDateSeasonStart($date): string {
    $date = self::createPhpDateTime($date);
    $date_season = self::getDateSeason($date);
    $date_month = $date->format('m');
    $date_year = $date->format('Y');

    // If month equal yanuary or february
    if ($date_month < 3) {
      return ($date_year - 1) . '-12-01';
    }

    return $date_year . '-' . sprintf('%02d', $date_season * 3) . '-01';
  }

  /**
   * Return TRUE if $date1 greater than $date2.
   *
   * Example:
   * <code>
   * DateHelper::dateIsGreaterThan('2021-10-01', '2021-01-01'); // TRUE
   * DateHelper::dateIsGreaterThan('2021-01-01', '2021-10-01'); // FALSE
   * </code>
   *
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date1
   * @param string|integer|DrupalDateTime|PhpDateTimeInterface $date2
   */
  public static function dateIsGreaterThan($date1, $date2): bool {
    $date1_timestamp = self::getDateTimestamp($date1);
    $date2_timestamp = self::getDateTimestamp($date2);
    return $date1_timestamp > $date2_timestamp;
  }

}
