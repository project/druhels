<?php

namespace Drupal\druhels;

use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

class UserHelper {

  /**
   * Return TRUE if current user has role.
   */
  public static function currentUserHasRole(string $role): bool {
    return in_array($role, \Drupal::currentUser()->getRoles());
  }

  /**
   * Return TRUE if current user is administrator.
   */
  public static function currentUserIsAdmin(): bool {
    return self::currentUserHasRole('administrator');
  }

  /**
   * Return admin role.
   */
  public static function getAdminRole(): string {
    return current(
      \Drupal::entityQuery('user_role')
        ->condition('is_admin', TRUE)
        ->accessCheck(FALSE)
        ->range(0, 1)
        ->execute()
    );
  }

  /**
   * Return admin user entity.
   */
  public static function getAdminUser(): UserInterface {
    $admin_user_id = current(
      \Drupal::entityQuery('user')
        ->condition('status', 1)
        ->condition('roles', self::getAdminRole())
        ->accessCheck(FALSE)
        ->range(0, 1)
        ->execute()
    );
    return User::load($admin_user_id);
  }

}
