<?php

namespace Drupal\druhels;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\file\FileInterface;

class EntityHelper {

  /**
   * Return field label. Supported base and configurable fields.
   */
  public static function getFieldLabel(string $entity_type_id, string $bundle_name, string $field_name): string {
    $entity_field_manager = \Drupal::service('entity_field.manager'); /** @var EntityFieldManagerInterface $entity_field_manager */
    return $entity_field_manager->getFieldDefinitions($entity_type_id, $bundle_name)[$field_name]->getLabel();
  }

  /**
   * Return list field allowed values.
   *
   * Example:
   * <code>
   * $allowed_values = EntityHelper::getListFieldAllowedValues('node', 'page', 'field_list_integer');
   * // [
   * //   0 => 'Foo',
   * //   1 => 'Bar',
   * // ]
   * </code>
   */
  public static function getListFieldAllowedValues(string $entity_type_id, string $bundle_name, string $field_name): ?array {
    $field_manager = \Drupal::service('entity_field.manager'); /** @var EntityFieldManagerInterface $field_manager */
    $fields_definitions = $field_manager->getFieldDefinitions($entity_type_id, $bundle_name);
    if (isset($fields_definitions[$field_name])) {
      $field_definition = $fields_definitions[$field_name];
      $field_storage_definition = $field_definition->getFieldStorageDefinition();
      return options_allowed_values($field_storage_definition);
    }
    return NULL;
  }

  /**
   * Return value label for List field.
   *
   * Example:
   * <code>
   * $node->set('field_list', 'foo');
   * $value_label = EntityHelper::getListFieldValueLabel($node->get('field_list')->first());
   * // Foo
   * </code>
   *
   * @param FieldItemInterface|FieldItemListInterface $field_item
   *
   * @return mixed
   */
  public static function getListFieldValueLabel($field_item): ?string {
    if ($field_item instanceof FieldItemListInterface) {
      $field_item = $field_item->first();
    }

    if ($field_item) {
      $allowed_values = options_allowed_values($field_item->getFieldDefinition()->getFieldStorageDefinition(), $field_item->getEntity());
      $value = $field_item->value;
      return ($value !== NULL && isset($allowed_values[$value])) ? $allowed_values[$value] : $value;
    }

    return NULL;
  }

  /**
   * Return field values as array.
   *
   * Example:
   * <code>
   * $node->set('field_strings', ['Foo', 'Bar']);
   * $values = $node->get('field_strings')->getValues(); // [['value' => 'Foo'], ['value' => 'Bar']]
   * $main_values = EntityHelper::getFieldValues($node->get('field_strings'), 'value'); // ['Foo', 'Bar']
   * </code>
   */
  public static function getFieldValues(FieldItemListInterface $items, string $value_property_name = NULL): array {
    $values = [];

    if (!$items->isEmpty()) {
      if (!$value_property_name) {
        $value_property_name = $items->first()->mainPropertyName();
      }

      foreach ($items as $item) {
        $values[] = $item->{$value_property_name};
      }
    }

    return $values;
  }

  /**
   * Return field value.
   *
   * @return mixed
   */
  public static function getExistsFieldValue(FieldableEntityInterface $entity, string $field_name, string $value_name = 'value') {
    return $entity->hasField($field_name) ? $entity->get($field_name)->{$value_name} : NULL;
  }

  /**
   * Return referenced entities. This analogue "$entity->get('field_reference')->referencedEntities()" but array keys it's entities ids, not deltas.
   *
   * @return EntityInterface[]
   */
  public static function getReferencedEntities(EntityReferenceFieldItemListInterface $items): array {
    $referenced_entities = [];

    foreach ($items as $item) {
      if ($referenced_entity = $item->entity) {
        $referenced_entities[$referenced_entity->id()] = $referenced_entity;
      }
    }

    return $referenced_entities;
  }

  /**
   * Return referenced entities field values.
   */
  public static function getReferencedEntitiesFieldValues(EntityReferenceFieldItemListInterface $items, string $field_name, string $value_property_name = NULL): array {
    $values = [];

    foreach ($items as $item) {
      if ($child_entity = $item->entity) {
        $values = array_merge($values, self::getFieldValues($child_entity->get($field_name), $value_property_name));
      }
    }

    return $values;
  }

  /**
   * Return TRUE if bundle has field.
   */
  public static function bundleHasField(string $entity_type_id, string $bundle_name, string $field_name): bool {
    $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
    return $field_storage && in_array($bundle_name, $field_storage->getBundles());
  }

  /**
   * Return bundle field definitions by field type.
   *
   * @param string $entity_type_id
   * @param string $bundle_name
   * @param string $field_type
   *
   * @return FieldDefinitionInterface[] Key is field machine name
   */
  public static function getBundleFieldDefinitionsByType(string $entity_type_id, string $bundle_name, string $field_type): array {
    $entity_field_manager = \Drupal::service('entity_field.manager'); /** @var EntityFieldManager $entity_field_manager */
    $field_definitions = $entity_field_manager->getFieldDefinitions($entity_type_id, $bundle_name);
    return array_filter($field_definitions, function ($field_definition) use ($field_type) {
      /** @var FieldDefinitionInterface $field_definition */
      return $field_definition->getType() == $field_type;
    });
  }

  /**
   * Return entity build array. Array does not contain fields, they generate in #pre_render.
   * If need array with builded fields use viewWithFields()
   *
   * @param EntityInterface $entity Entity object
   */
  public static function view(EntityInterface $entity, string $view_mode = 'full'): array {
    $entity_type_id = $entity->getEntityTypeId();
    return \Drupal::entityTypeManager()->getViewBuilder($entity_type_id)->view($entity, $view_mode);
  }

  /**
   * Return entity build array with fields.
   */
  public static function viewWithFields(EntityInterface $entity, string $view_mode = 'full'): array {
    $build = self::view($entity, $view_mode);

    foreach ($build['#pre_render'] as $callable) {
      $build = call_user_func($callable, $build);
    }

    unset($build['#pre_render']);

    return $build;
  }

  /**
   * Delete multiple entities.
   *
   * @param integer[] $entity_ids
   * @param string $entity_type_id Entity type id
   */
  public static function deleteMultiple(array $entity_ids, string $entity_type_id) {
    $entity_storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    $entities = $entity_storage->loadMultiple($entity_ids);
    return $entity_storage->delete($entities);
  }

  /**
   * Return TRUE if entity field value changed.
   */
  public static function fieldHasAffectingChanges(EntityInterface $entity, string $field_name): bool {
    $original_entity = $entity->original; /** @var EntityInterface $original_entity */
    $field_items = $entity->get($field_name);
    $original_field_items = $original_entity->get($field_name);

    return $field_items->hasAffectingChanges($original_field_items, $field_items->getLangcode());
  }

  /**
   * Return TRUE if field has value.
   *
   * @param FieldItemListInterface $items
   * @param mixed $value
   * @param string|null $value_property_name
   */
  public static function fieldHasValue(FieldItemListInterface $items, $value, string $value_property_name = NULL): bool {
    /** @var FieldItemInterface $item */
    foreach ($items as $item) {
      if (!$value_property_name) {
        $value_property_name = $item::mainPropertyName();
      }
      if ($item->{$value_property_name} == $value) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Return TRUE if referenced entities field has value.
   */
  public static function referencedEntitiesFieldHasValue(EntityReferenceFieldItemListInterface $parent_items, string $child_field_name, $child_field_value, string $child_value_property_name = NULL): bool {
    /** @var EntityReferenceItem $parent_item */
    foreach ($parent_items as $parent_item) {
      $child_entity = $parent_item->entity; /** @var FieldableEntityInterface $child_entity */
      if ($child_entity && self::fieldHasValue($child_entity->get($child_field_name), $child_field_value, $child_value_property_name)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Return content entity types.
   *
   * @return ContentEntityTypeInterface[] Key is entity type id
   */
  public static function getContentEntityTypes(): array {
    return array_filter(\Drupal::entityTypeManager()->getDefinitions(), function (EntityTypeInterface $entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });
  }

  /**
   * Return TRUE if current page is entity canonical page.
   */
  public static function isEntityPage(string $entity_type_id = NULL): bool {
    if (preg_match('/^entity\.(.+)\.canonical$/', DrupalHelper::getCurrentRouteName(), $matches)) {
      if ($entity_type_id) {
        return $matches[1] == $entity_type_id;
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Return current entity from route.
   */
  public static function getCurrentEntity(RouteMatchInterface $route_match = NULL): ?EntityInterface {
    if (!$route_match) {
      $route_match = \Drupal::routeMatch();
    }

    if (preg_match('/^entity\.(.+)\.canonical$/', DrupalHelper::getCurrentRouteName($route_match), $matches)) {
      $entity_type_id = $matches[1];
      $entity = $route_match->getParameter($entity_type_id); /** @var EntityInterface $entity */

      if ($entity && is_numeric($entity)) {
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity);
      }

      return $entity;
    }

    return NULL;
  }

  /**
   * Return translated entity.
   */
  public static function getTranslatedEntity(TranslatableInterface $entity, $langcode = NULL): EntityInterface {
    if (!$langcode) {
      $langcode = DrupalHelper::getCurrentLangcode();
    }

    return $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $entity;
  }

  /**
   * Set third party settings.
   */
  public static function setThirdPartySettings(ThirdPartySettingsInterface $entity, string $module_name, array $settings): void {
    foreach ($settings as $key => $value) {
      $entity->setThirdPartySetting($module_name, $key, $value);
    }
  }

  /**
   * Return entity data table name.
   */
  public static function getEntityDataTableName(string $entity_type_id): string {
    static $cache = [];

    if (!array_key_exists($entity_type_id, $cache)) {
      $entity_storage = \Drupal::entityTypeManager()->getStorage($entity_type_id); /** @var SqlContentEntityStorage $entity_storage */
      $cache[$entity_type_id] = $entity_storage->getDataTable();
    }

    return $cache[$entity_type_id];
  }

  /**
   * Return field data table name.
   */
  public static function getFieldDataTableName(string $entity_type_id, string $field_name): string {
    static $cache = [];

    $cache_key = $entity_type_id . ':' . $field_name;

    if (!isset($cache[$cache_key])) {
      $entity_storage = \Drupal::entityTypeManager()->getStorage($entity_type_id); /** @var SqlEntityStorageInterface $entity_storage */
      $entity_table_mapping = $entity_storage->getTableMapping(); /** @var DefaultTableMapping $entity_table_mapping */
      $cache[$cache_key] = $entity_table_mapping->getFieldTableName($field_name);
    }

    return $cache[$cache_key];
  }

  /**
   * Return "image field" default image info.
   */
  public static function getImageFieldDefaultImageInfo(string $entity_type_id, string $bundle_name, string $field_name): array {
    static $cache = [];

    $cache_key = $entity_type_id . ':' . $bundle_name . ':' . $field_name;

    if (!array_key_exists($cache_key, $cache)) {
      $field_definition = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle_name)[$field_name];
      $default_image_info = $field_definition->getSetting('default_image');

      if (empty($default_image_info['uuid']) && $field_definition instanceof FieldConfigInterface) {
        $default_image_info = $field_definition->getFieldStorageDefinition()->getSetting('default_image');
      }

      $cache[$cache_key] = $default_image_info;
    }

    return $cache[$cache_key];
  }

  /**
   * Return "image field" default image entity.
   */
  public static function getImageFieldDefaultImageEntity(string $entity_type_id, string $bundle_name, string $field_name): ?FileInterface {
    $default_image_info = self::getImageFieldDefaultImageInfo($entity_type_id, $bundle_name, $field_name);

    if (!empty($default_image_info['uuid'])) {
      return \Drupal::service('entity.repository')->loadEntityByUuid('file', $default_image_info['uuid']) ?: NULL;
    }

    return NULL;
  }

}
