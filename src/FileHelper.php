<?php

namespace Drupal\druhels;

use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\image\Entity\ImageStyle;

class FileHelper {

  /**
   * Return file entity object by file uri.
   *
   * Example:
   * <code>
   * $file = FileHelper::getFileEntityByUri('public://image.jpg');
   * </code>
   */
  public static function getFileEntityByUri(string $uri): ?FileInterface {
    $file_entity_storage = self::getFileStorage();
    if ($file_entities = $file_entity_storage->loadByProperties(['uri' => $uri])) {
      return current($file_entities);
    }
    return NULL;
  }

  /**
   * Create file entity if not exists.
   *
   * Example:
   * <code>
   * $file = FileHelper::createFileEntityByUri('public://image.jpg');
   * </code>
   */
  public static function createFileEntityByUri(string $uri, $values = [], bool $save = TRUE): FileInterface {
    $file = self::getFileEntityByUri($uri);

    if (!$file) {
      $file = File::create([
        'uri' => $uri,
        'filename' => basename($uri),
      ] + $values);

      if ($save) {
        $file->save();
      }
    }

    return $file;
  }

  /**
   * Return file extension without dot.
   *
   * Example:
   * <code>
   * $file_extension = FileHelper::getFileExtension('/path/image.jpg');
   * // 'jpg'
   * </code>
   */
  public static function getFileExtension(string $filename): string {
    $file_extension = pathinfo($filename, PATHINFO_EXTENSION);
    return explode('?', $file_extension)[0];
  }

  /**
   * Return image style url.
   *
   * Example:
   * <code>
   * $image_url = FileHelper::getImageStyleUrl('public://image.jpg', 'wide');
   * // '/sites/default/files/styles/wide/public/image.jpg?itok=6CEABsqq'
   * </code>
   *
   * @param string $image_path The path or URI to the original image.
   * @param string $style_name Style name.
   */
  public static function getImageStyleUrl(string $image_path, string $style_name, bool $absolute_url = FALSE): string {
    if ($style_name && ($image_style = ImageStyle::load($style_name))) {
      $image_style_url = $image_style->buildUrl($image_path);
    }
    else {
      $image_style_url = file_create_url($image_path);
    }

    return $absolute_url ? $image_style_url : file_url_transform_relative($image_style_url);
  }

  /**
   * Return file entity storage.
   */
  public static function getFileStorage(): FileStorageInterface {
    return \Drupal::entityTypeManager()->getStorage('file');
  }

}
