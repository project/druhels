<?php

namespace Drupal\druhels;

use Drupal\Component\Utility\NestedArray;

class ArrayHelper {

  /**
   * Rename keys to column value.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::renameKeysToColumnValue([
   *   0 => ['id' => 1, 'title' => 'First'],
   *   1 => ['id' => 3, 'title' => 'Second'],
   * ], 'id');
   * // [
   * //   1 => ['id' => 1, 'title' => 'First'],
   * //   3 => ['id' => 3, 'title' => 'Second'],
   * // ]
   * </code>
   *
   * @param array $array Source array
   * @param mixed $column_key Column key
   */
  public static function renameKeysToColumnValue(array $array, mixed $column_key): array {
    $new_array = array();

    foreach ($array as $key => $values) {
      if (isset($values[$column_key])) {
        $new_array[$values[$column_key]] = $values;
      }
    }

    return $new_array;
  }

  /**
   * Insert element before/after key.
   *
   * @param array $source_array Input array
   * @param mixed $source_array_key Array key
   * @param array $inserted_element Inserted element
   * @param string $insert_type Insertion type 'before' or 'after'
   */
  public static function insert(array $source_array, mixed $source_array_key, array $inserted_element, string $insert_type): array {
    $key_position = array_search($source_array_key, array_keys($source_array), TRUE);

    if ($key_position !== FALSE) {
      $offset = ($insert_type == 'after') ? 1 : 0;
      $array_before = array_slice($source_array, 0, $key_position + $offset, TRUE);
      $array_after = array_slice($source_array, $key_position + $offset, count($source_array), TRUE);
      $array = $array_before + $inserted_element + $array_after;
    }
    else {
      $array = $source_array + $inserted_element;
    }

    return $array;
  }

  /**
   * Insert element before key.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::insertBefore(
   *   ['foo' => 'Foo', 'baz' => 'Baz'],
   *   'baz',
   *   ['bar' => 'Bar']
   * );
   * // [
   * //   'foo' => 'Foo',
   * //   'bar' => 'Bar',
   * //   'baz' => 'Baz',
   * // ]
   * </code>
   *
   * @param array $source_array Input array
   * @param mixed $source_array_key Array key
   * @param array $inserted_element Inserted element
   */
  public static function insertBefore(array $source_array, mixed $source_array_key, array $inserted_element): array {
    return self::insert($source_array, $source_array_key, $inserted_element, 'before');
  }

  /**
   * Insert element after key.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::insertAfter(
   *   ['foo' => 'Foo', 'baz' => 'Baz'],
   *   'foo',
   *   ['bar' => 'Bar']
   * );
   * // [
   * //   'foo' => 'Foo',
   * //   'bar' => 'Bar',
   * //   'baz' => 'Baz',
   * // ]
   * </code>
   *
   * @param array $source_array Input array
   * @param mixed $source_array_key Array key
   * @param array $inserted_element Inserted element
   */
  public static function insertAfter(array $source_array, mixed $source_array_key, array $inserted_element): array {
    return self::insert($source_array, $source_array_key, $inserted_element, 'after');
  }

  /**
   * Insert element after element with specific value.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::insertAfterValue([0 => 'Foo', 1 => 'Baz'], 'Foo', [2 => 'Bar']);
   * // [
   * //   0 => 'Foo',
   * //   2 => 'Bar',
   * //   1 => 'Baz',
   * // ]
   * </code>
   *
   * @param array $source_array Input array
   * @param mixed $source_element_value Searched element value
   * @param array $inserted_element Inserted element
   */
  public static function insertAfterValue(array $source_array, mixed $source_element_value, array $inserted_element): array {
    $source_array_key = array_search($source_element_value, $source_array);
    if ($source_array_key !== FALSE) {
      return self::insertAfter($source_array, $source_array_key, $inserted_element);
    }
    return $source_array + $inserted_element;
  }

  /**
   * Move element before key.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::moveBefore([
   *   'foo' => 'Foo',
   *   'bar' => 'Bar',
   *   'baz' => 'Baz',
   * ], 'baz', 'bar');
   * // [
   * //   'foo' => 'Foo',
   * //   'baz' => 'Baz',
   * //   'bar' => 'Bar',
   * // ]
   * </code>
   *
   * @param array $array Input array
   * @param mixed $key Element key to move
   * @param mixed $before_key Element key before
   */
  public static function moveBefore(array $array, mixed $key, mixed $before_key): array {
    if (array_key_exists($before_key, $array)) {
      $element = $array[$key];
      unset($array[$key]);
      $array = self::insertBefore($array, $before_key, [$key => $element]);
    }

    return $array;
  }

  /**
   * Rename key.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::renameKey(['foo' => 'Foo', 'bar' => 'Bar'], 'foo', 'f');
   * // [
   * //   'f' => 'Foo',
   * //   'bar' => 'Bar',
   * // ]
   * </code>
   *
   * @param array $array Input array
   * @param mixed $old_key Old key name
   * @param mixed $new_key New key name
   */
  public static function renameKey(array $array, mixed $old_key, mixed $new_key): array {
    if (array_key_exists($old_key, $array)) {
      $array = self::insertAfter($array, $old_key, array($new_key => $array[$old_key]));
      unset($array[$old_key]);
    }
    return $array;
  }

  /**
   * Remove empty elements from array ('', 0, null, false).
   *
   * Example:
   * <code>
   * $array = ArrayHelper::removeEmptyElements([
   *   'foo' => 'Foo',
   *   'bar' => '',
   *   'baz' => NULL,
   *   'qwe' => 'Qwe',
   * ]);
   * // [
   * //   'foo' => 'Foo',
   * //   'qwe' => 'Qwe',
   * // ]
   * </code>
   */
  public static function removeEmptyElements(array $array, bool $recursive = FALSE): array {
    foreach ($array as $key => $value) {
      if ($recursive && is_array($value) && $value) {
        $array[$key] = $value = self::removeEmptyElements($array[$key]);
      }
      if (!$value) {
        unset($array[$key]);
      }
    }

    return $array;
  }

  /**
   * Remove element from array by key. Alternative unset() but returning array.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::removeElementByKey([
   *   'foo' => 'Foo',
   *   'bar' => 'Bar',
   * ], 'foo');
   * // ['bar' => 'Bar']
   * </code>
   *
   * @param array $array Input array
   * @param mixed $key Element key
   */
  public static function removeElementByKey(array $array, mixed $key): array {
    unset($array[$key]);
    return $array;
  }

  /**
   * Remove elements from array by keys. Alternative unset($array[...], $array[...], ...) but returning array.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::removeElementsByKey([
   *   'foo' => 'Foo',
   *   'bar' => 'Bar',
   *   'baz' => 'baz',
   * ], ['foo', 'baz']);
   * // ['bar' => 'Bar']
   * </code>
   *
   * @param array $array Input array
   * @param array $keys Array of keys for remove
   */
  public static function removeElementsByKeys(array $array, array $keys): array {
    foreach ($keys as $key) {
      if (array_key_exists($key, $array)) {
        unset($array[$key]);
      }
    }

    return $array;
  }

  /**
   * Remove elements from $array which one keys are not in $keys.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::removeExtraElements([
   *   'foo' => 'Foo',
   *   'bar' => 'bar',
   *   'baz' => 'baz',
   * ], ['foo', 'baz']);
   * // [
   * //   'foo' => 'Foo',
   * //   'baz' => 'Baz',
   * // ]
   * </code>
   *
   * @param array $array Input array
   * @param array $keys_to_leave Array of keys to leave
   */
  public static function removeExtraElements(array $array, array $keys_to_leave): array {
    return array_intersect_key($array, array_fill_keys($keys_to_leave, NULL));
  }

  /**
   * Remove element from nested array and clear branch.
   *
   * Example:
   * <code>
   * // Remove without cleaning
   * $array = ArrayHelper::removeElementFromNestedArray([
   *   'foo' => [
   *     0 => 123,
   *   ],
   *   'bar' => 234,
   * ], ['foo', 0], FALSE);
   * // [
   * //   'foo' => [],
   * //   'bar' => 234,
   * // ]
   *
   * // Remove with cleaning
   * $array = ArrayHelper::removeElementFromNestedArray([
   *   'foo' => [
   *     0 => 123,
   *   ],
   *   'bar' => 234,
   * ], ['foo', 0], TRUE);
   * // [
   * //   'bar' => 234,
   * // ]
   * </code>
   */
  public static function removeElementFromNestedArray(array $array, array $keys, bool $clear_branch = FALSE): array {
    NestedArray::unsetValue($array, $keys);

    if ($clear_branch) {
      for ($i = count($keys) - 1; $i > 0; $i--) {
        $current_keys = array_slice($keys, 0, $i);
        if (NestedArray::getValue($array, $current_keys) === []) {
          NestedArray::unsetValue($array, $current_keys);
        }
        else {
          break;
        }
      }
    }

    return $array;
  }

  /**
   * Search value and replace to new value.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::searchAndReplace([0 => 'Foo', 1 => 'Bar'], 'Bar', 'Baz');
   * // [
   * //   0 => 'Foo',
   * //   1 => 'Baz',
   * // ]
   * </code>
   */
  public static function searchAndReplace(array $array, mixed $search_value, mixed $new_value): array {
    foreach (array_keys($array, $search_value, TRUE) as $key) {
      $array[$key] = $new_value;
    }

    return $array;
  }

  /**
   * Return text lines in array.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::getTextLines("Foo\nBar");
   * // ['Foo', 'Bar']
   * </code>
   *
   * @param string $text Text
   */
  public static function getTextLines(string $text, bool $remove_empty = TRUE, bool $trim = TRUE): array {
    $lines = preg_split("/\r\n|\n|\r/", $text);

    if ($trim) {
      $lines = array_map('trim', $lines);
    }
    if ($remove_empty) {
      $lines = self::removeEmptyElements($lines);
    }

    return $lines;
  }

  /**
   * Search in two-dimensional array and return element key.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::searchInTwodimArray([
   *   'foo' => ['id' => 1, 'title' => 'Foo item'],
   *   'bar' => ['id' => 2, 'title' => 'Bar item'],
   * ], 'title', 'Bar item');
   * // 'bar'
   * </code>
   *
   * @param array $array Two-dimensional array
   * @param mixed $column_key Second dimension key
   * @param mixed $value Search value
   * @param bool $strict
   * @param string $return_mode 'first', 'last' or 'all'
   *
   * @return mixed Key or keys array
   */
  public static function searchInTwodimArray(array $array, mixed $column_key, mixed $value, bool $strict = FALSE, string $return_mode = 'first'): mixed {
    $found_keys = [];

    foreach ($array as $key => $second_array) {
      if (array_key_exists($column_key, $second_array)) {
        if ($strict) {
          if ($second_array[$column_key] === $value) {
            if ($return_mode == 'first') {
              return $key;
            }
            $found_keys[] = $key;
          }
        }
        elseif ($second_array[$column_key] == $value) {
          if ($return_mode == 'first') {
            return $key;
          }
          $found_keys[] = $key;
        }
      }
    }

    if ($found_keys) {
      if ($return_mode == 'last') {
        return end($found_keys);
      }
      if ($return_mode == 'all') {
        return $found_keys;
      }
    }

    return FALSE;
  }

  /**
   * Return first key.
   *
   * Example:
   * <code>
   * $first_key = ArrayHelper::getFirstKey(['foo' => 'Foo', 'bar' => 'Bar']);
   * // 'foo'
   * </code>
   *
   * @return mixed
   */
  public static function getFirstKey(array $array): mixed {
    reset($array);
    return key($array);
  }

  /**
   * Return last key.
   *
   * Example:
   * <code>
   * $first_key = ArrayHelper::getFirstKey(['foo' => 'Foo', 'bar' => 'Bar']);
   * // 'bar'
   * </code>
   *
   * @return mixed
   */
  public static function getLastKey(array $array): mixed {
    end($array);
    return key($array);
  }

  /**
   * Sort array by second array values order.
   *
   * Example:
   * <code>
   * $sorted_array = ArrayHelper::sortBySecondArrayValues(
   *   ['bar' => 'Bar', 'foo' => 'Foo'],
   *   ['foo', 'bar']
   * );
   * // ['foo' => 'Foo', 'bar' => 'Bar']
   * </code>
   */
  public static function sortBySecondArrayValues(array $array, array $second_array): array {
    $sorted_array = array();

    foreach ($second_array as $value) {
      if (array_key_exists($value, $array)) {
        $sorted_array[$value] = $array[$value];
      }
    }

    $sorted_array += $array;

    return $sorted_array;
  }

  /**
   * Sort array by second array keys order.
   *
   * Example:
   * <code>
   * $sorted_array = ArrayHelper::sortBySecondArrayKeys(
   *   ['bar' => 'Bar', 'foo' => 'Foo'],
   *   ['foo' => '...', 'bar' => '...']
   * );
   * // ['foo' => 'Foo', 'bar' => 'Bar']
   * </code>
   */
  public static function sortBySecondArrayKeys(array $array, array $second_array): array {
    $sorted_array = array();

    foreach (array_keys($second_array) as $key) {
      if (array_key_exists($key, $array)) {
        $sorted_array[$key] = $array[$key];
      }
    }

    $sorted_array += $array;

    return $sorted_array;
  }

  /**
   * Implode multidimensional array.
   *
   * Example:
   * <code>
   * $string = ArrayHelper::implodeMultiArray([['foo', 'Foo'], ['bar', 'Ba']], "\n", '|');
   * // "foo|Foo\nbar|Bar"
   * </code>
   */
  public static function implodeMultiArray(array $array, string $lines_separator, string $values_separator): string {
    foreach ($array as $key => $values) {
      if (is_array($values)) {
        $array[$key] = implode($values_separator, $values);
      }
    }

    return implode($lines_separator, $array);
  }

  /**
   * Format array as "key: value" string list.
   *
   * Example:
   * <code>
   * $string = ArrayHelper::formatArrayAsKeyValueList([
   *   1 => 'Foo',
   *   2 => 'Bar',
   * ]);
   * // "1: Foo\n2: Bar"
   * </code>
   */
  public static function formatArrayAsKeyValueList(array $array, string $key_value_separator = ': ', string $row_separator = "\n"): string {
    $list = [];

    foreach ($array as $key => $value) {
      $list[] = $key . $key_value_separator . $value;
    }

    return implode($row_separator, $list);
  }

  /**
   * Format "key: value" string as array.
   *
   * Example:
   * <code>
   * $array = ArrayHelper::formatKeyValueListAsArray("1:Foo\n2:Bar", ':');
   * // ['1' => 'Foo', '2' => 'Bar']
   * </code>
   */
  public static function formatKeyValueListAsArray(string $list, string $key_value_separator): array {
    $array = [];

    foreach (self::getTextLines($list) as $line) {
      [$key, $value] = explode($key_value_separator, $line);
      $array[$key] = $value;
    }

    return $array;
  }

  /**
   * Return TRUE if all keys exists in $array.
   *
   * Example:
   * <code>
   * $result = ArrayHelper::keysExists([
   *   'foo' => 'Foo',
   *   'bar' => 'Bar',
   *   'baz' => 'Baz',
   * ], ['foo', 'baz']);
   * // TRUE
   *
   * $result = ArrayHelper::keysExists([
   *   'foo' => 'Foo',
   *   'bar' => 'Bar',
   * ], ['foo', 'qwe']);
   * // FALSE
   * </code>
   *
   * @param array $array
   * @param array $keys
   */
  public static function keysExists(array $array, array $keys): bool {
    foreach ($keys as $key) {
      if (!array_key_exists($key, $array)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Return TRUE if all values in column is empty.
   *
   * Example:
   * <code>
   * $result = ArrayHelper::columnIsEmpty([
   *   ['id' => 1, 'title' => 'One'],
   *   ['id' => 2, 'title' => ''],
   * ], 'title');
   * // FALSE
   *
   * $result = ArrayHelper::columnIsEmpty([
   *   ['id' => 1, 'title' => ''],
   *   ['id' => 2, 'title' => ''],
   * ], 'title');
   * // TRUE
   * </code>
   */
  public static function columnIsEmpty(array $array, mixed $column_name): bool {
    foreach ($array as $row) {
      if (!empty($row[$column_name])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Return array value by position index.
   *
   * Example:
   * <code>
   * $second_element = ArrayHelper::getElementByIndex(['foo' => 'Foo', 'bar' => 'Bar'], 1);
   * // 'Bar'
   * </code>
   *
   * @return mixed
   */
  public static function getValueByIndex(array $array, int $index): mixed {
    $keys = array_keys($array);
    if (array_key_exists($index, $keys)) {
      return $array[$keys[$index]];
    }
    return NULL;
  }

  /**
   * Return TRUE if $array1 contains at least one value from $array2.
   *
   * Example:
   * <code>
   * ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['bar', 'baz']); // TRUE
   * ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['baz', 'qwe']); // FALSE
   * </code>
   */
  public static function arrayContainsAtLeastOneValueFromOtherArray(array $array1, array $array2): bool {
    return count(array_intersect($array1, $array2)) > 0;
  }

  /**
   * Return random elements.
   */
  public static function randomSlice(array $array, int $num): array {
    $random_keys = array_rand($array, $num);
    return array_intersect_key($array, array_flip($random_keys));
  }

  /**
   * Convert CSV file to array.
   * Use iterator_to_array() to get native array.
   *
   * @return \Generator
   */
  public static function csvToArray(string $uri, string $column_separator = ';', bool $header = TRUE): \Generator {
    $file_handle = fopen($uri, 'rb');

    if ($header) {
      $header_row = fgetcsv($file_handle, 0, $column_separator);
    }

    $row_index = 0;

    try {
      while (($row = fgetcsv($file_handle, 0, $column_separator)) !== FALSE) {
        yield $row_index++ => $header ? array_combine($header_row, $row) : $row;
      }
    }
    finally {
      fclose($file_handle);
    }
  }

  /**
   * Format array as attribites.
   *
   * Example:
   * <code>
   * ArrayHelper::formatArrayAsAttributes(['id' => 'foo', 'class' => 'bar']); // ['id' => 'foo', 'class' => ['bar']]
   * </code>
   */
  public static function formatArrayAsAttributes(array $array): array {
    $attributes = [];

    foreach ($array as $attribute_name => $attribute_value) {
      if ($attribute_name == 'class' && !is_array($attribute_value)) {
        $attributes[$attribute_name] = [$attribute_value];
      }
      else {
        $attributes[$attribute_name] = $attribute_value;
      }
    }

    return $attributes;
  }

}
