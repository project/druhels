<?php

namespace Drupal\druhels;

use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\improvements\ImprovementsBlockViewBuilder;

class BlockHelper {

  /**
   * Return block render-array by block plugin id.
   * Notice - don't support render cache.
   */
  public static function viewByBlockPluginId(string $block_plugin_id, bool $wrapper = TRUE, string $label = '', bool $add_cacheable_metadata = TRUE, BlockPluginInterface &$block_plugin = NULL): array {
    $block_manager = \Drupal::service('plugin.manager.block');
    $block_plugin = $block_manager->createInstance($block_plugin_id, []);
    $access_result = $block_plugin->access(\Drupal::currentUser());

    if (
      (is_object($access_result) && $access_result->isAllowed()) ||
      (is_bool($access_result) && $access_result)
    ) {
      if ($block_build = $block_plugin->build()) {
        if ($wrapper) {
          $block_build = [
            '#theme' => 'block',
            '#configuration' => [
              'label' => $label,
              'label_display' => BlockPluginInterface::BLOCK_LABEL_VISIBLE,
            ] + $block_plugin->getConfiguration(),
            '#plugin_id' => $block_plugin->getPluginId(),
            '#base_plugin_id' => $block_plugin->getBaseId(),
            '#derivative_plugin_id' => $block_plugin->getDerivativeId(),
            'content' => $block_build,
          ];
        }

        if ($add_cacheable_metadata) {
          CacheableMetadata::createFromObject($block_plugin)->applyTo($block_build);
        }

        return $block_build;
      }
    }

    return [];
  }

  /**
   * Return block render-array by block plugin id using native BlockViewBuilder.
   * Notice - if $wrapper=TRUE then will be used render cache.
   */
  public static function viewByBlockPluginIdUsingNativeBuilder(string $block_plugin_id, bool $wrapper = TRUE, string $label = '', string $block_config_id = NULL): array {
    $block_config = Block::create([
      'id' => $block_config_id ?: $block_plugin_id,
      'plugin' => $block_plugin_id,
      'settings' => [
        'label' => $label,
        'label_display' => BlockPluginInterface::BLOCK_LABEL_VISIBLE,
      ],
    ]);

    // Build block render-array with #lazy_builder and #cache['keys']
    $block_lazy_build = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block_config);

    // Build block render-array with '#theme'=>'block' without content
    $block_build = ImprovementsBlockViewBuilder::buildPreRenderableBlock($block_config, \Drupal::service('module_handler'));

    // Merge render-arrays caches
    $block_build_cacheable_metadata = CacheableMetadata::createFromRenderArray($block_lazy_build)
      ->merge(CacheableMetadata::createFromRenderArray($block_build));
    $block_build_cacheable_metadata->applyTo($block_build);
    $block_build['#cache']['keys'] = $block_lazy_build['#cache']['keys'];

    if ($wrapper) {
      //unset($block_build['#contextual_links']); ???
      return $block_build;
    }

    // Generate content
    if (isset($block_build['#pre_render'])) {
      foreach ($block_build['#pre_render'] as $callable) {
        $block_build = $callable($block_build);
      }
    }

    // Set content #cache
    $block_build_cacheable_metadata
      ->merge(CacheableMetadata::createFromRenderArray($block_build['content']))
      ->applyTo($block_build['content']);

    return $block_build['content'] ?? [];
  }

  /**
   * Return full block render-array by block config id.
   */
  public static function viewByBlockConfigId(string $block_config_id): array {
    $block_config = Block::load($block_config_id);
    return \Drupal::entityTypeManager()->getViewBuilder('block')->view($block_config);
  }

  /**
   * Return block render-array by content block id.
   */
  public static function viewByContenBlockId(string $content_block_id, bool $wrapper = TRUE, string $label = ''): array {
    if ($content_block = BlockContent::load($content_block_id)) {
      $content_block_build = self::viewByBlockPluginId('block_content:' . $content_block->uuid(), $wrapper, $label);

      $content_block_build['#contextual_links'] = [
        'block_content' => [
          'route_parameters' => ['block_content' => $content_block_id],
        ],
      ];
      $content_block_build['#id'] = $content_block->get('type')->target_id . '-block-' . $content_block_id;

      return $content_block_build;
    }

    return [
      '#markup' => t('Content block "@id" not exists.', ['@id' => $content_block_id]),
    ];
  }

}
