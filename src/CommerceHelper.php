<?php

namespace Drupal\druhels;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

class CommerceHelper {

  /**
   * Return user cart.
   */
  public static function getCart(AccountInterface $account = NULL, string $order_type = 'default'): ?OrderInterface {
    return \Drupal::service('commerce_cart.cart_provider')->getCart($order_type, NULL, $account);
  }

  /**
   * Return user carts.
   *
   * @return OrderInterface[]
   */
  public static function getCarts(AccountInterface $account = NULL, bool $only_filled = FALSE): array {
    /** @see \Drupal\commerce_cart\Plugin\Block\CartBlock::build() */
    $carts = \Drupal::service('commerce_cart.cart_provider')->getCarts($account); /** @var OrderInterface[] $carts */

    if ($only_filled) {
      return array_filter($carts, function (OrderInterface $cart) {
        return $cart->hasItems() && $cart->cart->value;
      });
    }

    return $carts;
  }

  /**
   * Return carts total price.
   */
  public static function getCartsTotalPrice(AccountInterface $account = NULL): Price {
    $carts_total_price = new Price(0, self::getDefaultCurrencyCode());

    if ($carts = self::getCarts($account)) {
      foreach ($carts as $cart) {
        if ($cart_total_price = $cart->getTotalPrice()) {
          $carts_total_price = $carts_total_price->add($cart_total_price);
        }
      }
    }

    return $carts_total_price;
  }

  /**
   * Clear carts.
   */
  public static function clearCarts(AccountInterface $account = NULL): void {
    foreach (self::getCarts($account) as $cart) {
      $cart->delete();
    }
  }

  /**
   * Return current store default currency code.
   *
   * Example:
   * <code>
   * $default_currency_code = CommerceHelper::getDefaultCurrencyCode();
   * // 'USD'
   * </code>
   */
  public static function getDefaultCurrencyCode(): string {
    $current_store = \Drupal::service('commerce_store.current_store')->getStore();
    return $current_store->getDefaultCurrencyCode();
  }

  /**
   * Return order total quantity.
   */
  public static function getOrderTotalQuantity(OrderInterface $order): int {
    $quantity = 0;

    foreach ($order->getItems() as $order_item) {
      $quantity += (int)$order_item->getQuantity();
    }

    return $quantity;
  }

  /**
   * Return carts items total quantity.
   */
  public static function getCartsTotalQuantity(): int {
    $quantity = 0;

    foreach (self::getCarts() as $cart) {
      $quantity += self::getOrderTotalQuantity($cart);
    }

    return $quantity;
  }

  /**
   * Return carts items count.
   */
  public static function getCartsItemsCount(): int {
    $count = 0;

    foreach (self::getCarts() as $cart) {
      $count += count($cart->getItems());
    }

    return $count;
  }

  /**
   * Return formatted price.
   *
   * Example:
   * <code>
   * $formatted_price = CommerceHelper::formatPrice(new Price(123, 'USD'));
   * // '$123'
   * </code>
   *
   * @param Price|array $price Price object or array
   */
  public static function formatPrice($price, array $options = []): string {
    if (!$price) {
      return '';
    }
    if (is_string($price)) {
      return $price;
    }

    if ($price instanceof Price) {
      $price = $price->toArray();
    }
    if (is_array($price) && isset($price['currency_code'], $price['number'])) {
      return \Drupal::service('commerce_price.currency_formatter')->format($price['number'], $price['currency_code'], $options);
    }

    return (string)$price;
  }

  /**
   * Return TRUE if current page is product page.
   */
  public static function isProductPage(string $product_type = NULL, RouteMatchInterface $route_match = NULL, bool $clear_cache = FALSE): bool {
    static $current_product_type;

    if ($clear_cache) {
      $current_product_type = NULL;
    }

    if ($current_product_type === NULL) {
      if (!$route_match) {
        $route_match = \Drupal::routeMatch();
      }

      if ($route_match->getRouteName() == 'entity.commerce_product.canonical') {
        $current_product_type = $route_match->getParameter('commerce_product')->bundle();
      }
      else {
        $current_product_type = '';
      }
    }

    return $product_type ? ($current_product_type == $product_type) : (bool)$current_product_type;
  }

  /**
   * Return current product id.
   */
  public static function getCurrentProductId(RouteMatchInterface $route_match = NULL): ?int {
    if (!$route_match) {
      $route_match = \Drupal::routeMatch();
    }
    return $route_match->getRawParameter('commerce_product');
  }

  /**
   * Return current product.
   */
  public static function getCurrentProduct(RouteMatchInterface $route_match = NULL): ?ProductInterface {
    if ($product_id = self::getCurrentProductId($route_match)) {
      return Product::load($product_id);
    }
    return NULL;
  }

  /**
   * Return product render array.
   */
  public static function productView(ProductInterface $product, string $view_mode = 'full'): array {
    return \Drupal::entityTypeManager()->getViewBuilder('commerce_product')->view($product, $view_mode);
  }

  /**
   * Return TRUE if order has product variation.
   */
  public static function orderHasVariation(OrderInterface $order, int $variation_id): bool {
    foreach ($order->getItems() as $order_item) {
      if ($order_item->getPurchasedEntityId() == $variation_id) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Return TRUE if cart has product variation.
   */
  public static function cartsHasVariation(int $variation_id): bool {
    foreach (self::getCarts() as $cart) {
      if (self::orderHasVariation($cart, $variation_id)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Return order items by variation id.
   *
   * @return OrderItemInterface[]
   */
  public static function getOrderItemsByVariationId(OrderInterface $order, int $variation_id): array {
    $matching_items = [];

    foreach ($order->getItems() as $order_item) {
      if ($order_item->getPurchasedEntityId() == $variation_id) {
        $matching_items[] = $order_item;
      }
    }

    return $matching_items;
  }

  /**
   * Return carts items by variation id.
   */
  public static function getCartsItemsByVariationId(int $variation_id): array {
    $matching_items = [];

    foreach (self::getCarts() as $cart) {
      $matching_items = array_merge($matching_items, self::getOrderItemsByVariationId($cart, $variation_id));
    }

    return $matching_items;
  }

  /**
   * Return cart item quantity by variation id.
   */
  public static function getCartItemQuantityByVariationId(int $variation_id): int {
    $quantity = 0;

    foreach (self::getCartsItemsByVariationId($variation_id) as $order_item) {
      $quantity += $order_item->getQuantity();
    }

    return $quantity;
  }

  /**
   * Add entity to cart.
   */
  public static function addToCart(PurchasableEntityInterface $purchasable_entity, int $quantity): void {
    $cart = self::getCart() ?: \Drupal::service('commerce_cart.cart_provider')->createCart('default');
    \Drupal::service('commerce_cart.cart_manager')->addEntity($cart, $purchasable_entity, $quantity);
  }

}
