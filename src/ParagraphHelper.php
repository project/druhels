<?php

namespace Drupal\druhels;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\paragraphs\ParagraphInterface;

class ParagraphHelper {

  /**
   * Return root entity.
   */
  public static function getRootEntity(ParagraphInterface $paragraph): ?ContentEntityInterface {
    while ($parent_entity = $paragraph->getParentEntity()) {
      if ($parent_entity instanceof ParagraphInterface) {
        $paragraph = $parent_entity;
      }
      else {
        break;
      }
    }

    return $parent_entity;
  }

}
