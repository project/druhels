<?php

namespace Drupal\druhels;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;

class TaxonomyHelper {

  /**
   * Return term id by name.
   */
  public static function getTermIdByName(string $vocabulary_machine_name, string $term_name, int $parent_id = NULL): ?int {
    static $cache = [];

    $cache_key = $vocabulary_machine_name . ':' . $term_name . ':' . $parent_id;

    if (!isset($cache[$cache_key])) {
      $query = self::getTermStorage()
        ->getQuery()
        ->condition('vid', $vocabulary_machine_name)
        ->condition('name', $term_name)
        ->accessCheck(FALSE)
        ->range(0, 1);

      if ($parent_id !== NULL) {
        $query->condition('parent', $parent_id);
      }

      $result = $query->execute();

      $cache[$cache_key] = $result ? (int)current($result) : NULL;
    }

    return $cache[$cache_key];
  }

  /**
   * Return term by name.
   */
  public static function getTermByName(string $vocabulary_machine_name, string $term_name, int $parent_id = NULL): ?TermInterface {
    if ($term_id = self::getTermIdByName($vocabulary_machine_name, $term_name, $parent_id)) {
      return Term::load($term_id);
    }

    return NULL;
  }

  /**
   * Create term and return term entity object. If term with this name exists then return it.
   */
  public static function createTerm(string $vocabulary_machine_name, string $term_name, int $parent_id = 0, array $values = [], bool &$created = NULL): ?TermInterface {
    if (!$term_name) {
      return NULL;
    }

    $term = self::getTermByName($vocabulary_machine_name, $term_name, $parent_id);

    if (!$term) {
      $term = Term::create([
        'name' => trim($term_name),
        'vid' => $vocabulary_machine_name,
        'parent' => $parent_id,
      ] + $values);
      $term->save();
      $created = TRUE;
    }
    else {
      $created = FALSE;
    }

    return $term;
  }

  /**
   * Create terms by names.
   *
   * Example:
   * <code>
   * $terms = TaxonomyHelper::createTerms(['Term 1', 'Term 2']);
   * // [
   * //   term_1_id => term_1_entity,
   * //   term_2_id => term_2_entity,
   * // ]
   * </code>
   */
  public static function createTerms(string $vocabulary_machine_name, array $terms_name, int $parent_id = 0, array $term_values = []): array {
    $result = [];

    foreach ($terms_name as $term_name) {
      $term = self::createTerm($vocabulary_machine_name, $term_name, $parent_id, $term_values);
      $result[(int)$term->id()] = $term;
    }

    return $result;
  }

  /**
   * Return term depth.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * $cat_1_depth = TaxonomyHelper::getTermDepth(cat_1_id); // 0
   * $cat_1_1_depth = TaxonomyHelper::getTermDepth(cat_1_1_id); // 1
   * $non_exist_cat_depth = TaxonomyHelper::getTermDepth(0); // NULL
   * </code>
   *
   * @paran int|TermInterface $term Term id or object
   */
  public static function getTermDepth($term): ?int {
    if ($term && is_numeric($term)) {
      $term = Term::load($term);
    }
    if (!$term) {
      return NULL;
    }

    $term_depth = 0;
    while ($term = $term->get('parent')->entity) {
      $term_depth++;
    }

    return $term_depth;
  }

  /**
   * Return terms by names hierarchy.
   *
   * Example:
   * <code>
   * $terms = TaxonomyHelper::getTermsByNamesHierarchy('category', ['Cat 1', 'Cat 1.2', 'Cat 1.2.1']);
   * // [
   * //   cat_1_id => term_entity,
   * //   cat_1_2_id => term_entity,
   * //   cat_1_2_1_id => term_entity,
   * // ]
   * </code>
   *
   * @return TermInterface[] Key is term id
   */
  public static function getTermsByNamesHierarchy(string $vocabulary_machine_name, array $names): array {
    $parent_id = 0;
    $terms = [];

    foreach ($names as $name) {
      if ($current_term = self::getTermByName($vocabulary_machine_name, $name, $parent_id)) {
        $terms[$current_term->id()] = $current_term;
        $parent_id = $current_term->id();
      }
      else {
        return [];
      }
    }

    return $terms;
  }

  /**
   * Create terms by names hierarchy.
   * Example:
   * <code>
   * $terms = TaxonomyHelper::createTermsByNamesHierarchy(['Term 1', 'Term 1.1']);
   * // [
   * //   term_1_id   => term_entity,
   * //   term_1_1_id => term_entity,
   * // ]
   * </code>
   *
   * @param string $vocabulary_machine_name Vocabulary machine name
   * @param array $names Array of terms names
   *
   * @return TermInterface[] Key is term id
   */
  public static function createTermsByNamesHierarchy(string $vocabulary_machine_name, array $names, int &$created_count = NULL): array {
    $terms = [];
    $parent_id = 0;
    $created_count = 0;

    foreach ($names as $term_name) {
      if ($term = self::createTerm($vocabulary_machine_name, $term_name, $parent_id, [], $created)) {
        $terms[$term->id()] = $term;
        $parent_id = $term->id();
        if ($created) {
          $created_count++;
        }
      }
    }

    return $terms;
  }

  /**
   * Return vocabulary terms sorted by weight. It's analogue of $term_storage->loadTree() but array keys is term ids.
   * Notice - used $term_storage->loadTree().
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getTermsByVocabulary('category', 0, 1);
   * // [
   * //   cat_1_id => cat_1_object,
   * //   cat_2_id => cat_2_object,
   * // ]
   * </code>
   *
   * @return \stdClass[]|TermInterface[] Key is term id
   */
  public static function getTermsByVocabulary(string $vocabulary_machine_name, int $parent_id = 0, int $max_depth = NULL, bool $load_entities = FALSE): array {
    $term_storage = self::getTermStorage();
    $terms = $term_storage->loadTree($vocabulary_machine_name, $parent_id, $max_depth, $load_entities);
    $terms_formatted = [];

    foreach ($terms as $term) {
      $term_id = $load_entities ? $term->id() : $term->tid;
      $terms_formatted[$term_id] = $term;
    }

    return $terms_formatted;
  }

  /**
   * Return all child terms regardless of depth. It's analogue of $term_storage->loadTree() but array keys is term ids.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // ----- Cat 1.1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getAllChildTerms('category', cat_1_id);
   * // [
   * //   cat_1_1_id   => cat_1_1_object,
   * //   cat_1_1_1_id => cat_1_1_1_object,
   * //   cat_1_2_id   => cat_1_2_object,
   * // ]
   * </code>
   *
   * @return \stdClass[]|TermInterface[] Key is term id
   */
  public static function getAllChildTerms(string $vocabulary_machine_name, int $term_id, bool $load_entities = FALSE): array {
    return self::getTermsByVocabulary($vocabulary_machine_name, $term_id, NULL, $load_entities);
  }

  /**
   * Return all child terms ids regardless of depth.
   * Notice - used $term_storage->loadTree().
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // ----- Cat 1.1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getAllChildTermsIds('category', cat_1_id);
   * // [
   * //   cat_1_1_id,
   * //   cat_1_1_1_id,
   * //   cat_1_2_id,
   * // ]
   * </code>
   *
   * @return integer[]
   */
  public static function getAllChildTermsIds(string $vocabulary_machine_name, int $term_id, bool $including_self = FALSE): array {
    static $cache = [];

    $cache_key = $vocabulary_machine_name . ':' . $term_id;
    if (!array_key_exists($cache_key, $cache)) {
      $cache[$cache_key] = array_keys(self::getAllChildTerms($vocabulary_machine_name, $term_id));
    }

    return $including_self ? [$term_id, ...$cache[$cache_key]] : $cache[$cache_key];
  }

  /**
   * Return direct child terms.
   * Notice - used $term_storage->loadTree().
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // ----- Cat 1.1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getDirectChildTerms('category', cat_1_id);
   * // [
   * //   cat_1_1_id => cat_1_1_object,
   * //   cat_1_2_id => cat_1_2_object,
   * // ]
   * </code>
   *
   * @return \stdClass[]|TermInterface[] Key is term id
   */
  public static function getDirectChildTerms(string $vocabulary_machine_name, int $term_id, bool $load_entities = FALSE): array {
    return self::getTermsByVocabulary($vocabulary_machine_name, $term_id, 1, $load_entities);
  }

  /**
   * Return direct child terms ids.
   * Notice - used $term_storage->loadTree().
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // ----- Cat 1.1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms_ids = TaxonomyHelper::getDirectChildTermsIds('category', cat_1_id);
   * // [cat_1_1_id, cat_1_2_id]
   * </code>
   *
   * @return integer[]
   */
  public static function getDirectChildTermsIds(string $vocabulary_machine_name, int $term_id, bool $inluding_self = FALSE): array {
    static $cache = [];

    $cache_key = $vocabulary_machine_name . ':' . $term_id;
    if (!array_key_exists($cache_key, $cache)) {
      $cache[$cache_key] = array_keys(self::getTermsByVocabulary($vocabulary_machine_name, $term_id, 1));
    }

    return $inluding_self ? [$term_id, ...$cache[$cache_key]] : $cache[$cache_key];
  }

  /**
   * Return TRUE if term has child terms.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * $has_childs = TaxonomyHelper::termHasChilds(cat_1_id); // TRUE
   * $has_childs = TaxonomyHelper::termHasChilds(cat_1_1_id); // FALSE
   * </code>
   */
  public static function termHasChilds(int $term_id, bool $reset_cache = FALSE): bool {
    static $cache = [];

    if ($reset_cache) {
      $cache = [];
    }

    if (!array_key_exists($term_id, $cache)) {
      $cache[$term_id] = \Drupal::entityQuery('taxonomy_term')
        ->condition('parent', $term_id)
        ->accessCheck(FALSE)
        ->count()
        ->execute() > 0;
    }

    return $cache[$term_id];
  }

  /**
   * Return term parent entity.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * $parent_term = TaxonomyHelper::getTermParent(cat_1_2_id); // cat_1_entity
   * $parent_term = TaxonomyHelper::getTermParent(cat_1_id); // NULL
   * </code>
   *
   * @param int|TermInterface $term Term id or object
   */
  public static function getTermParent($term): ?TermInterface {
    if ($term && is_numeric($term)) {
      $term = Term::load($term);
    }
    return $term ? $term->get('parent')->entity : NULL;
  }

  /**
   * Return term parent id.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * $parent_term_id = TaxonomyHelper::getTermParentId(cat_1_2_id); // cat_1_id
   * $parent_term_id = TaxonomyHelper::getTermParentId(cat_1_id); // NULL
   * </code>
   *
   * @param int|TermInterface $term Term id or object
   * @return int Parent term id
   */
  public static function getTermParentId(int|TermInterface $term): ?int {
    if ($term && is_numeric($term)) {
      $term = Term::load($term);
    }
    $parent_id = $term ? $term->get('parent')->target_id : NULL;
    return $parent_id ? (int)$parent_id : NULL;
  }

  /**
   * Return all term parents entities regardless of depth.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getAllParentsTerms(cat_1_1_id);
   * // [
   * //   cat_1_id => cat_1_entity,
   * //   cat_1_1_id => cat_1_1_entity,
   * // ]
   * </code>
   *
   * @return TermInterface[] Key is term id
   */
  public static function getAllParentsTerms(int $term_id, bool $including_self = TRUE): array {
    $term_with_all_parents = self::getTermStorage()->loadAllParents($term_id);

    if ($term_with_all_parents && !$including_self) {
      unset($term_with_all_parents[$term_id]);
    }
    elseif (!$term_with_all_parents && $including_self && ($term = Term::load($term_id))) {
      $term_with_all_parents[$term_id] = $term;
    }

    return array_reverse($term_with_all_parents, TRUE);
  }

  /**
   * Return all term raw parents regardless of depth.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms = TaxonomyHelper::getAllParentsTermsRaw(cat_1_1_id);
   * // [
   * //   cat_1_id => cat_1_stdclass,
   * //   cat_1_1_id => cat_1_1_stdclass,
   * // ]
   * </code>
   *
   * @return \stdClass[]
   */
  public static function getAllParentsTermsRaw(int $term_id, string $vocabulary_machine_name, bool $including_self = TRUE): array {
    $vocabulary_terms = self::getTermsByVocabulary($vocabulary_machine_name);
    $all_parents = [];

    if ($including_self && isset($vocabulary_terms[$term_id])) {
      $all_parents[$term_id] = $vocabulary_terms[$term_id];
    }

    do {
      $parent_id = (isset($vocabulary_terms[$term_id]) && $vocabulary_terms[$term_id]->parents[0])
        ? $vocabulary_terms[$term_id]->parents[0]
        : NULL;
      if ($parent_id && isset($vocabulary_terms[$parent_id])) {
        $all_parents = [$parent_id => $vocabulary_terms[$parent_id]] + $all_parents;
        $term_id = $parent_id;
      }
    } while ($parent_id);

    return $all_parents;
  }

  /**
   * Return all parents terms ids regardless of depth.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $terms_ids = TaxonomyHelper::getAllParentsTermsIds(cat_1_1_id, 'category'); // [cat_1_id, cat_1_1_id]
   * $terms_ids = TaxonomyHelper::getAllParentsTermsIds(cat_1_id, 'category', FALSE); // []
   * </code>
   *
   * @return integer[]
   */
  public static function getAllParentsTermsIds(int $term_id, string $vocabulary_machine_name, bool $including_self = TRUE): array {
    static $cache = [];

    if (!isset($cache[$term_id])) {
      $cache[$term_id] = array_keys(self::getAllParentsTermsRaw($term_id, $vocabulary_machine_name, FALSE));
    }

    return $including_self ? array_merge($cache[$term_id], [$term_id]) : $cache[$term_id];
  }

  /**
   * Return all parents terms name regardless of depth.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * // - Cat 2
   * $names = TaxonomyHelper::getAllParentsTermsNameRaw(cat_1_1_id, 'category');
   * // ['Cat 1', 'Cat 1.1']
   * </code>
   *
   * @return string[] Key is term id, value is term name
   */
  public static function getAllParentsTermsNameRaw(int $term_id, string $vocabulary_machine_name, bool $including_self = TRUE): array {
    static $cache = [];

    if (!isset($cache[$term_id])) {
      $all_parents = self::getAllParentsTermsRaw($term_id, $vocabulary_machine_name);
      $all_parents_name = [];

      foreach ($all_parents as $parent) {
        $all_parents_name[$parent->tid] = $parent->name;
      }

      $cache[$term_id] = $all_parents_name;
    }

    return $including_self ? $cache[$term_id] : array_diff_key($cache[$term_id], [$term_id => $term_id]);
  }

  /**
   * Return term direct parents ids. If term dont't have parents then return an empty array.
   *
   * @param int|TermInterface $term Term id or entity
   *
   * @return int[]
   */
  public static function getDirectParentsIds($term): array {
    if ($term && is_numeric($term)) {
      $term = Term::load($term);
    }
    if (!$term) {
      return [];
    }

    // Not used "EntityHelper::getFieldValues()" because "parent" field maybe contains "0"
    $parent_ids = [];
    foreach ($term->get('parent') as $item) {
      if ($target_id = $item->target_id) {
        $parent_ids[] = (int)$target_id;
      }
    }

    return $parent_ids;
  }

  /**
   * Return root parent term. Return self term if he doesn't have parents.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * $root_cat = TaxonomyHelper::getRootParentTerm(cat_1_2_id); // cat_1_entity
   * $root_cat = TaxonomyHelper::getRootParentTerm(cat_1_id); // cat_1_entity
   * </code>
   */
  public static function getRootParentTerm(int $term_id): ?TermInterface {
    $term_with_all_parents = self::getAllParentsTerms($term_id);
    return $term_with_all_parents ? current($term_with_all_parents) : NULL;
  }

  /**
   * Return root parent term id. Return self term if he doesn't have parents.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // --- Cat 1.1
   * // --- Cat 1.2
   * $root_cat_id = TaxonomyHelper::getRootParentTermId(cat_1_2_id); // cat_1_id
   * $root_cat_id = TaxonomyHelper::getRootParentTermId(cat_1_id); // cat_1_id
   * </code>
   */
  public static function getRootParentTermId(int $term_id): ?int {
    $root_parent_term = self::getRootParentTerm($term_id);
    return $root_parent_term ? $root_parent_term->id() : NULL;
  }

  /**
   * Return TRUE if current page is term page.
   *
   * @param string|string[] $vocabulary_machine_name Vocabulary machine name or names array
   */
  public static function isTermPage(string|array $vocabulary_machine_name = NULL, RouteMatchInterface $route_match = NULL): bool {
    static $current_vocabulary_machine_name;

    if ($current_vocabulary_machine_name === NULL) {
      if (DrupalHelper::getCurrentRouteName($route_match) == 'entity.taxonomy_term.canonical') {
        $term = self::getCurrentTerm($route_match);
        $current_vocabulary_machine_name = $term->bundle();
      }
      else {
        $current_vocabulary_machine_name = FALSE;
      }
    }

    if ($current_vocabulary_machine_name) {
      if ($vocabulary_machine_name) {
        if (!is_array($vocabulary_machine_name)) {
          $vocabulary_machine_name = [$vocabulary_machine_name];
        }
        return in_array($current_vocabulary_machine_name, $vocabulary_machine_name, TRUE);
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Return current term id from url.
   */
  public static function getCurrentTermId(RouteMatchInterface $route_match = NULL): ?int {
    static $current_term_id;

    if ($current_term_id === NULL) {
      if ($route_match === NULL) {
        $route_match = \Drupal::routeMatch();
      }

      $current_term_id = $route_match->getRawParameter('taxonomy_term') ?: 0;
      if ($current_term_id && !is_numeric($current_term_id)) {
        $current_term_id = 0;
      }
    }

    return $current_term_id ?: NULL;
  }

  /**
   * Return current term object from url.
   */
  public static function getCurrentTerm(RouteMatchInterface $route_match = NULL): ?TermInterface {
    if ($term_id = self::getCurrentTermId($route_match)) {
      /** @noinspection All */
      return Term::load($term_id);
    }
    return NULL;
  }

  /**
   * Return term id by term properties.
   *
   * Example:
   * <code>
   * $term = TaxonomyHelper::getTermIdByProperties([
   *   'name' => 'Cat 1',
   *   'vid' => 'category',
   * ]);
   * // cat_1_id
   * </code>
   */
  public static function getTermIdByProperties(array $properties): ?int {
    $term_query = \Drupal::entityQuery('taxonomy_term')->accessCheck(FALSE);
    $term_query->range(0, 1);

    foreach ($properties as $field_name => $field_value) {
      $term_query->condition($field_name, $field_value);
    }

    $result = $term_query->execute();

    return $result ? current($result) : NULL;
  }

  /**
   * Return term url object.
   *
   * Example:
   * <code>
   * $url_string = TaxonomyHelper::getTermUrl(123)->toString();
   * // '/taxonomy/term/123'
   * </code>
   */
  public static function getTermUrl(int $term_id, array $options = []): Url {
    return Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term_id], $options);
  }

  /**
   * Return term storage.
   */
  public static function getTermStorage(): TermStorageInterface {
    /** @noinspection All */
    return \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  }

  /**
   * Return terms name from database.
   *
   * Example:
   * <code>
   * $names = TaxonomyHelper::getTermsName([1, 2]);
   * // [
   * //   1 => 'Term name 1',
   * //   2 => 'Term name 2',
   * // ]
   * </code>
   *
   * @return string[] Key is term id, value is term name
   */
  public static function getTermsName(array $terms_ids, bool $use_term_load = TRUE): array {
    static $cache = [];

    $uncached_terms_ids = array_diff($terms_ids, array_keys($cache));

    if ($uncached_terms_ids) {
      if ($use_term_load) {
        foreach (Term::loadMultiple($uncached_terms_ids) as $term_id => $term) {
          $cache[$term_id] = $term->label();
        }
      }
      else {
        $cache += \Drupal::database()->select('taxonomy_term_field_data', 't')
          ->fields('t', ['tid', 'name'])
          ->condition('t.tid', $uncached_terms_ids, 'IN')
          ->execute()
          ->fetchAllKeyed();
      }
    }

    return array_intersect_key($cache, array_flip($terms_ids));
  }

  /**
   * Return TRUE if term has direct parent.
   *
   * Example:
   * <code>
   * // - Cat 1
   * // -- Cat 1.1
   * // -- Cat 1.2
   * TaxonomyHelper::termHasDirectParent($cat_1); // FALSE
   * TaxonomyHelper::termHasDirectParent($cat_1, cat_1_1_id); // FALSE
   * TaxonomyHelper::termHasDirectParent($cat_1_1); // TRUE
   * TaxonomyHelper::termHasDirectParent($cat_1_1, cat_1_id); // TRUE
   * TaxonomyHelper::termHasDirectParent($cat_1_1, cat_1_2_id); // FALSE
   * </code>
   */
  public static function termHasDirectParent(TermInterface $term, int $parent_id = NULL): bool {
    $term_direct_parents_ids = self::getDirectParentsIds($term);

    if ($parent_id === NULL) {
      return empty($term_direct_parents_ids);
    }
    if ($parent_id === 0) {
      return !$term_direct_parents_ids;
    }
    return in_array($parent_id, $term_direct_parents_ids);
  }

  /**
   * Convert terms flat array to tree.
   *
   * Example:
   * <code>
   * $tree = TaxonomyHelper::formatTermsAsTree([$term_1, $term_1_1, $term_1_2]);
   * // [
   * //   1 => [
   * //     'term' => $term_1,
   * //     'children' => [
   * //       2 => [
   * //         'term' => $term_1_1,
   * //       ],
   * //       3 => [
   * //         'term' => $term_1_2,
   * //       ],
   * //     ],
   * //   ],
   * // ]
   * </code>
   *
   * @param TermInterface[] $terms Terms entities array
   *
   * @return array
   */
  public static function formatTermsAsTree(array $terms, int $parent_id = NULL): array {
    $tree = [];

    foreach ($terms as $term) {
      if (self::termHasDirectParent($term, $parent_id)) {
        $tree[$term->id()] = [
          'term' => $term,
          'children' => self::formatTermsAsTree($terms, (int)$term->id()),
        ];
      }
    }

    return $tree;
  }

}
