<?php

namespace Drupal\druhels;

class StringHelper {

  /**
   * Remove double spaces from string.
   *
   * Example:
   * <code>
   * $string = StringHelper::removeDoubleSpaces('foo  bar');
   * // 'foo bar'
   * </code>
   */
  public static function removeDoubleSpaces(string $string): string {
    return preg_replace('/\s+/', ' ', $string);
  }

  /**
   * Strip tags and clean text.
   */
  public static function stripTags(string $string): string {
    $string = str_replace('&nbsp;', ' ', $string);
    $string = html_entity_decode($string);
    $string = trim(strip_tags($string));
    $string = preg_replace('/(\r|\n)/', ' ', $string);
    return self::removeDoubleSpaces($string);
  }

  /**
   * Match pattern and return one or more matches.
   *
   * Example:
   * <code>
   * $string = StringHelper::pregMatch('/^(.+),/', 'Hello, World!', 1);
   * // 'Hello'
   * </code>
   *
   * @param string $pattern Regular expression
   * @param string $text Text string
   * @param integer|NULL $index Matches index to return
   *
   * @return string|array|NULL
   */
  public static function pregMatch(string $pattern, string $text, int $index = NULL) {
    if (preg_match($pattern, $text, $matches)) {
      if ($index === NULL) {
        return $matches;
      }
      if (isset($matches[$index])) {
        return $matches[$index];
      }
    }
    return NULL;
  }

  /**
   * Convert string to float.
   *
   * Example:
   * <code>
   * $float = StringHelper::convertToFloat('123,45');
   * // 123.45
   * </code>
   */
  public static function convertToFloat(string $string): float {
    $string = trim($string);
    $string = strtr($string, [
      ',' => '.',
      ' ' => '',
    ]);
    return (float)$string;
  }

  /**
   * Remove html comments from string.
   */
  public static function removeHtmlComments(string $string): string {
    return preg_replace('/<!--(.|\s)*?-->/', '', $string);
  }

}
