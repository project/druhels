<?php

namespace Drupal\druhels;

use Drupal\Component\Utility\Html;

class SeoHelper {

  /**
   * Attach html_head tag.
   *
   * Example:
   * <code>
   * SeoHelper::attachHtmlHead($build, 'shortcut_link', 'link', [
   *   'rel' => 'shortcut icon',
   *   'href' => '/favicon.ico',
   * ]);
   * </code>
   */
  public static function attachHtmlHead(array &$build, string $attachment_name, string $tag_name, array $attributes): void {
    $tag = [
      '#tag' => $tag_name,
      '#attributes' => $attributes,
    ];
    $build['#attached']['html_head'][] = [$tag, $attachment_name];
  }

  /**
   * Attach metatag to render array.
   *
   * Example:
   * <code>
   * SeoHelper::attachMetatag($build, 'viewport_meta', 'viewport', 'width=device-width');
   * </code>
   */
  public static function attachMetatag(array &$build, string $attachment_name, string $meta_name, string $content): void {
    self::attachHtmlHead($build, $attachment_name, 'meta', [
      'name' => $meta_name,
      'content' => ['#plain_text' => $content],
    ]);
  }

  /**
   * Attach metatag <meta name="robots" content="noindex" /> to render array.
   *
   * Example:
   * <code>
   * SeoHelper::attachMetatag($build, 'viewport_meta', 'viewport', 'width=device-width');
   * </code>
   *
   * @TODO Add test
   */
  public static function attachMetatagNoindex(array &$build): void {
    self::attachMetatag($build, 'meta_robots_noindex', 'robots', 'noindex');
  }

  /**
   * Attach canonical link.
   *
   * Example:
   * <code>
   * SeoHelper::attachCanonical($build, 'http://example.com/');
   * </code>
   *
   * @TODO Add test
   */
  public static function attachCanonical(array &$build, string $url): void {
    self::attachHtmlHead($build, 'canonical_url', 'link', [
      'rel' => 'canonical',
      'href' => $url,
    ]);
  }

  /**
   * Attach JSON-LD to render array.
   *
   * Example:
   * <code>
   * SeoHelper::attachJsonld($build, 'product_jsonld', [
   *   @context' => 'https://schema.org/',
   *  '@type' => 'Product',
  *   'sku' => 'PRODUCT',
   * ]);
   * </code>
   */
  public static function attachJsonld(array &$build, string $attachment_name, array $array, int $weight = 1000): void {
    $array = ArrayHelper::removeEmptyElements($array, TRUE);

    $element = [
      '#tag' => 'script',
      '#attributes' => [
        'type' => 'application/ld+json',
      ],
      '#value' => json_encode($array, JSON_UNESCAPED_UNICODE),
      '#weight' => $weight,
    ];

    $build['#attached']['html_head'][] = [$element, $attachment_name];
  }

  /**
   * Render schema.org data.
   */
  public static function renderSchemaorgData(array $data): string {
    $output = '';

    foreach ($data as $name => $content) {
      if (strpos($name, '#') !== 0) {
        // Nested
        if (is_array($content) && isset($content['#itemtype'])) {
          $output .= "
            <div itemprop=\"$name\" itemscope itemtype=\"http://schema.org/{$content['#itemtype']}\">
              " . self::renderSchemaorgData($content) . "
            </div>
          ";
        }
        // Custom tag
        elseif (is_array($content)) {
          $attributes = [];
          foreach ($content as $attribute_name => $attribute_value) {
            if ($attribute_name != '#tag') {
              $attributes[] = substr($attribute_name, 1) . '="' . Html::escape($attribute_value) . '"';
            }
          }

          $tag = $content['#tag'] ?? 'meta';
          $output .= '<' . $tag . ' itemprop="' . $name . '" ' . implode(' ', $attributes) . ' />' . "\n";
        }
        // Meta
        else {
          $output .= '<meta itemprop="' . $name . '" content="' . Html::escape($content) . '" />' . "\n";
        }
      }
    }

    return $output;
  }

}
