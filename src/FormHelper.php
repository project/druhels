<?php

namespace Drupal\druhels;

use Drupal\Core\Render\Element;

class FormHelper {

  /**
   * Remove system elements from GET form.
   */
  public static function cleanGetForm(array $form): array {
    unset($form['form_id']);
    unset($form['form_build_id']);
    unset($form['form_token']);

    return $form;
  }

  /**
   * Move labels to placeholder.
   */
  public static function moveLabelsToPlaceholder(array &$form, bool $mark_required_fields = TRUE): void {
    foreach (Element::children($form) as $key) {
      if (is_array($form[$key])) {
        if (isset($form[$key]['#type']) && in_array($form[$key]['#type'], ['textfield', 'textarea', 'email', 'password'])) {
          if (empty($form[$key]['#placeholder']) && !empty($form[$key]['#title'])) {
            $form[$key]['#placeholder'] = $form[$key]['#title'];

            if ($mark_required_fields && !empty($form[$key]['#required'])) {
              $form[$key]['#placeholder'] .= ' *';
            }
          }
        }
        else {
          self::moveLabelsToPlaceholder($form[$key]);
        }
      }
    }
  }

}
