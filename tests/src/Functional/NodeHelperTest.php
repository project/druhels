<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\druhels\NodeHelper;
use Drupal\node\NodeStorageInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * @coversDefaultClass \Drupal\druhels\NodeHelper
 */
class NodeHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
    'block',
    'datetime',
    'node',
    'filter',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->placeBlock('system_messages_block', [
      'id' => 'system_messages_block',
    ]);
    $this->placeBlock('system_info_block', [
      'id' => 'system_info',
      'label' => 'System info',
    ]);

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->createContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
  }

  /**
   * Tests runner.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::isNodePage
   */
  private function _testIsNodePage(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is node page: 0');
    $web_assert->pageTextContains('Is node "page" page: 0');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is node page: 1');
    $web_assert->pageTextContains('Is node "page" page: 1');

    $article = $this->createNode(['type' => 'article']);
    $this->drupalGetEntityPage($article);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is node page: 1');
    $web_assert->pageTextContains('Is node "page" page: 0');

    // Clean
    $this->deleteEntities($node, $article);
  }

  /**
   * @covers ::getCurrentNodeId
   */
  private function _testGetCurrentNodeId(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current node id: ~');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current node id: ' . $node->id());

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getCurrentNode
   */
  private function _testGetCurrentNode(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current node: ~');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current node: ' . $node->id());

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::view
   */
  private function _testView(): void {
    $node = $this->createNode();

    $node_build = NodeHelper::view($node);
    $this->assertArrayHasKey('#theme', $node_build);
    $this->assertSame('node', $node_build['#theme']);

    $node_build = NodeHelper::view($node->id());
    $this->assertArrayHasKey('#theme', $node_build);
    $this->assertSame('node', $node_build['#theme']);

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getNodeStorage
   */
  private function _testGetNodeStorage(): void {
    $this->assertInstanceOf(NodeStorageInterface::class, NodeHelper::getNodeStorage());
  }

  /**
   * @covers ::getNodeUrl
   */
  private function _testGetNodeUrl(): void {
    $this->assertSame('/node/1', NodeHelper::getNodeUrl(1)->toString());
    $this->assertSame('/node/99999', NodeHelper::getNodeUrl(99999)->toString());
  }

}
