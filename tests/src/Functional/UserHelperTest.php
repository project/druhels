<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\user\Entity\User;

/**
 * @coversDefaultClass \Drupal\druhels\UserHelper
 */
class UserHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
    'block',
    'user',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $root_user = User::load($this->rootUser->id());
    $root_user->addRole('administrator');
    $root_user->save();

    $this->placeBlock('system_messages_block', [
      'id' => 'system_messages_block',
    ]);
    $this->placeBlock('system_info_block', [
      'id' => 'system_info',
      'label' => 'System info',
    ]);
  }

  /**
   * Tests runner.
   */
  public function testMain(): void {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::currentUserHasRole
   */
  private function _testCurrentUserHasRole(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current user has role administrator: 0');

    $this->drupalLoginAsRoot();
    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current user has role administrator: 1');

    // Clean
    $this->drupalLogout();
  }

  /**
   * @covers ::currentUserIsAdmin
   */
  private function _testCurrentUserIsAdmin(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current user is admin: 0');

    $this->drupalLoginAsRoot();
    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current user is admin: 1');

    // Clean
    $this->drupalLogout();
  }

}
