<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;

/**
 * @coversDefaultClass \Drupal\druhels\SeoHelper
 */
class SeoHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests runner.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::attachHtmlHead
   */
  private function _testAttachHtmlHead(): void {
    $web_assert = $this->assertSession();
    $this->drupalGet(Url::fromRoute('druhels_test.attach_html_head'));
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'head link[rel="test-link"]');
  }

  /**
   * @covers ::attachMetatag
   */
  private function _testAttachMetatag(): void {
    $web_assert = $this->assertSession();
    $this->drupalGet(Url::fromRoute('druhels_test.attach_metatag'));
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'head meta[name="test-metatag"][content="foo bar baz"]');
  }

  /**
   * @covers ::attachJsonld
   */
  private function _testAttachJsonld(): void {
    $web_assert = $this->assertSession();
    $this->drupalGet(Url::fromRoute('druhels_test.attach_jsonld'));
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'script[type="application/ld+json"]');
  }

  /**
   * @covers ::renderSchemaorgData
   */
  private function _testRenderSchemaorgData(): void {
    $web_assert = $this->assertSession();
    $this->drupalGet(Url::fromRoute('druhels_test.shemaorg_data'));
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'meta[itemprop="foo"][content="bar"]');
  }

}
