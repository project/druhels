<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\Core\Render\RendererInterface;
use Drupal\druhels\BlockHelper;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;

/**
 * @coversDefaultClass \Drupal\druhels\BlockHelper
 */
class BlockHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;
  use BlockCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'block',
    'block_content',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  protected RendererInterface $renderer;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->renderer = $this->container->get('renderer');
  }

  /**
   * Tests runner.
   */
  public function testMain(): void {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::viewByBlockPluginId
   */
  private function _testViewByBlockPluginId(): void {
    // Wrapper and title
    $block_build = BlockHelper::viewByBlockPluginId('system_powered_by_block', TRUE, 'Test block title');
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringContainsString('<div>', $block_output);
    $this->assertStringContainsString('<h2>Test block title</h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);

    // Wrapper without title
    $block_build = BlockHelper::viewByBlockPluginId('system_powered_by_block');
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringContainsString('<div>', $block_output);
    $this->assertStringNotContainsString('<h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);

    // Without wrapper
    $block_build = BlockHelper::viewByBlockPluginId('system_powered_by_block', FALSE);
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringNotContainsString('<div>', $block_output);
    $this->assertStringNotContainsString('<h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);
  }

  /**
   * @covers ::viewByBlockPluginIdUsingNativeBuilder
   */
  private function _testViewByBlockPluginIdUsingNativeBuilder(): void {
    // Wrapper and title
    $block_build = BlockHelper::viewByBlockPluginIdUsingNativeBuilder('system_powered_by_block', TRUE, 'Test block title');
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringContainsString('<div id="block-system-powered-by-block', $block_output);
    $this->assertStringContainsString('<h2>Test block title</h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);

    // Wrapper without title
    $this->clearRenderCache();
    $block_build = BlockHelper::viewByBlockPluginIdUsingNativeBuilder('system_powered_by_block');
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringContainsString('<div id="block-system-powered-by-block', $block_output);
    $this->assertStringNotContainsString('<h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);

    // Without wrapper
    $this->clearRenderCache();
    $block_build = BlockHelper::viewByBlockPluginIdUsingNativeBuilder('system_powered_by_block', FALSE);
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringNotContainsString('<div id="block-system-powered-by-block', $block_output);
    $this->assertStringNotContainsString('<h2>', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);
  }

  /**
   * @covers ::viewByBlockConfigId
   */
  private function _testViewByBlockConfigId(): void {
    $this->placeBlock('system_powered_by_block', ['id' => 'test_system_powered_by_block']);
    $block_build = BlockHelper::viewByBlockConfigId('test_system_powered_by_block');
    $block_output = $this->renderer->renderRoot($block_build);
    $this->assertStringContainsString('<div id="block-test-system-powered-by-block"', $block_output);
    $this->assertStringContainsString('Powered by ', $block_output);
  }

  /**
   * @covers ::viewByContenBlockId
   */
  private function _testViewByContenBlockId(): void {
    $this->createBlockContentType();
    $content_block = $this->createBlockContent([
      'info' => 'Content block title',
      'body' => 'Content block body',
    ]);

    // Wrapper and title
    $content_block_build = BlockHelper::viewByContenBlockId($content_block->id(), TRUE, $content_block->label());
    $content_block_output = $this->renderer->renderRoot($content_block_build);
    $this->assertStringContainsString('<div id="block-basic-block-' . $content_block->id(), $content_block_output);
    $this->assertStringContainsString('<h2>Content block title</h2>', $content_block_output);
    $this->assertStringContainsString($content_block->get('body')->value, $content_block_output);

    // Wrapper without title
    $content_block_build = BlockHelper::viewByContenBlockId($content_block->id());
    $content_block_output = $this->renderer->renderRoot($content_block_build);
    $this->assertStringContainsString('<div id="block-basic-block-' . $content_block->id(), $content_block_output);
    $this->assertStringNotContainsString('<h2>', $content_block_output);
    $this->assertStringContainsString($content_block->get('body')->value, $content_block_output);

    // Without wrapper
    $content_block_build = BlockHelper::viewByContenBlockId($content_block->id(), FALSE);
    $content_block_output = $this->renderer->renderRoot($content_block_build);
    $this->assertStringNotContainsString('<div id="block-basic-block-' . $content_block->id(), $content_block_output);
    $this->assertStringNotContainsString('<h2>', $content_block_output);
    $this->assertStringContainsString($content_block->get('body')->value, $content_block_output);
  }

}
