<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\druhels\CommerceHelper;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\commerce_cart\Traits\CartBrowserTestTrait;
use Drupal\Tests\improvements\Traits\ImprovementsCommerceTestTrait;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Symfony\Component\Routing\Route;

/**
 * @coversDefaultClass \Drupal\druhels\CommerceHelper
 */
class CommerceHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;
  use ImprovementsCommerceTestTrait;
  use StoreCreationTrait;
  use CartBrowserTestTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'commerce',
    'commerce_cart',
    'commerce_checkout',
    'commerce_order',
    'commerce_price',
    'commerce_product',
    'commerce_store',
  ];

  protected CartProviderInterface $cartProvider;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected array $variations = [];

  protected array $products = [];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLoginAsRoot();
    $this->createStore('Default');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->cartProvider = $this->container->get('commerce_cart.cart_provider');

    $this->variations = [
      $this->createProductVariation('PROD1', new Price(12, 'USD')),
      $this->createProductVariation('PROD2', new Price(23, 'USD')),
    ];

    $this->products = [
      $this->createProduct('Product 1', [$this->variations[0]]),
      $this->createProduct('Product 2', [$this->variations[1]]),
    ];
  }

  /**
   * Tests runner.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::getDefaultCurrencyCode
   */
  private function _testGetDefaultCurrencyCode(): void {
    // @TODO Fix error
    $this->assertSame('USD', CommerceHelper::getDefaultCurrencyCode());
  }

  /**
   * @covers ::clearCarts
   */
  private function _testClearCarts(): void {
    $this->postAddToCart($this->products[0]);
    $this->clearCartCaches();
    $carts = $this->cartProvider->getCarts();
    $this->assertCount(1, $carts);
    $this->assertCount(1, current($carts)->getItems());

    CommerceHelper::clearCarts();
    $this->assertCount(0, $this->cartProvider->getCarts());
  }

  /**
   * @covers ::getCarts
   */
  private function _testGetCarts(): void {
    $this->assertCount(0, CommerceHelper::getCarts());

    $this->postAddToCart($this->products[0]);
    $this->clearCartCaches();
    $carts = CommerceHelper::getCarts();
    $this->assertCount(1, $carts);
    $this->assertCount(1, current($carts)->getItems());

    // Clear
    CommerceHelper::clearCarts();
  }

  /**
   * @covers ::getCartsTotalPrice
   */
  private function _testGetCartsTotalPrice(): void {
    $this->clearCartCaches();
    $this->assertSame('0', CommerceHelper::getCartsTotalPrice()->getNumber());

    $this->postAddToCart($this->products[0]);
    $this->clearCartCaches();
    $this->assertSame('12', CommerceHelper::getCartsTotalPrice()->getNumber());

    $this->postAddToCart($this->products[0]);
    $this->clearCartCaches();
    $this->assertSame('24', CommerceHelper::getCartsTotalPrice()->getNumber());

    $this->postAddToCart($this->products[1]);
    $this->clearCartCaches();
    $this->assertSame('47', CommerceHelper::getCartsTotalPrice()->getNumber());

    // Clear
    CommerceHelper::clearCarts();
  }

  /**
   * @covers ::getOrderTotalQuantity
   */
  private function _testGetOrderTotalQuantity(): void {
    $order_item1 = $this->createOrderItemByVariation($this->variations[0]);
    $order = $this->createOrder([$order_item1]);
    $this->assertSame(1, CommerceHelper::getOrderTotalQuantity($order));

    $order_item2 = $this->createOrderItemByVariation($this->variations[1], ['quantity' => 2]);
    $order->addItem($order_item2);
    $order->save();
    $this->assertSame(3, CommerceHelper::getOrderTotalQuantity($order));

    // Clean
    $this->deleteEntities($order);
  }

  /**
   * @covers ::getCartsTotalQuantity
   */
  private function _testGetCartsTotalQuantity(): void {
    $this->clearCartCaches();
    $this->assertSame(0, CommerceHelper::getCartsTotalQuantity());

    $this->postAddToCart($this->products[0]);
    $this->clearCartCaches();
    $this->assertSame(1, CommerceHelper::getCartsTotalQuantity());

    $this->postAddToCart($this->products[1]);
    $this->postAddToCart($this->products[1]);
    $this->clearCartCaches();
    $this->assertSame(3, CommerceHelper::getCartsTotalQuantity());
  }

  /**
   * @covers ::formatPrice
   */
  private function _testFormatPrice(): void {
    $this->assertSame('$123.00', CommerceHelper::formatPrice(new Price(123, 'USD')));
    $this->assertSame('$123.00', CommerceHelper::formatPrice(['number' => 123, 'currency_code' => 'USD']));
    $this->assertSame('$123.00', CommerceHelper::formatPrice('$123.00'));
  }

  /**
   * @covers ::isProductPage
   */
  private function _testIsProductPage(): void {
    $route_match = new RouteMatch(
      'entity.commerce_product.canonical',
      new Route('/product/{commerce_product}'),
      ['commerce_product' => $this->products[0]],
      ['commerce_product' => $this->products[0]->id()]
    );
    $this->assertSame(TRUE, CommerceHelper::isProductPage(NULL, $route_match));
    $this->assertSame(TRUE, CommerceHelper::isProductPage('default', $route_match));
    $this->assertSame(FALSE, CommerceHelper::isProductPage('test', $route_match));

    $route_match = new RouteMatch('<front>', new Route('/'));
    $this->assertSame(FALSE, CommerceHelper::isProductPage(NULL, $route_match, TRUE));
    $this->assertSame(FALSE, CommerceHelper::isProductPage('default', $route_match));
    $this->assertSame(FALSE, CommerceHelper::isProductPage('test', $route_match));
  }

  /**
   * @covers ::getCurrentProductId
   */
  private function _testGetCurrentProductId(): void {
    $route_match = new RouteMatch(
      'entity.commerce_product.canonical',
      new Route('/product/{commerce_product}'),
      ['commerce_product' => $this->products[0]],
      ['commerce_product' => $this->products[0]->id()]
    );
    $this->assertSame((int)$this->products[0]->id(), CommerceHelper::getCurrentProductId($route_match));

    $route_match = new RouteMatch('<front>', new Route('/'));
    $this->assertSame(NULL, CommerceHelper::getCurrentProductId($route_match));
  }

  /**
   * @covers ::getCurrentProduct
   */
  private function _testGetCurrentProduct(): void {
    $route_match = new RouteMatch(
      'entity.commerce_product.canonical',
      new Route('/product/{commerce_product}'),
      ['commerce_product' => $this->products[0]],
      ['commerce_product' => $this->products[0]->id()]
    );
    $this->assertSame($this->products[0]->id(), CommerceHelper::getCurrentProduct($route_match)->id());

    $route_match = new RouteMatch('<front>', new Route('/'));
    $this->assertSame(NULL, CommerceHelper::getCurrentProduct($route_match));
  }

  /**
   * @covers ::productView
   */
  private function _testProductView(): void {
    $build = CommerceHelper::productView($this->products[0]);
    $this->assertArrayHasKey('#theme', $build);
    $this->assertSame('commerce_product', $build['#theme']);
  }

}
