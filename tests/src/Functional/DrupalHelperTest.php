<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\druhels\DrupalHelper;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * @coversDefaultClass \Drupal\druhels\DrupalHelper
 */
class DrupalHelperTest extends BrowserTestBase {

  use AssertMailTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
    'block',
    'breakpoint',
    'datetime',
    'node',
    'filter',
    'path',
    'path_alias',
    'user',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
    ]);
    $this->placeBlock('system_messages_block', [
      'id' => 'system_messages_block',
    ]);
    $this->placeBlock('system_info_block', [
      'id' => 'system_info',
      'label' => 'System info',
    ]);

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);
  }

  /**
   * Tests runner.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::getCurrentPageTitle
   */
  private function _testGetCurrentPageTitle(): void {
    $web_assert = $this->assertSession();

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current title: ' . $node->label());

    $this->drupalGet(Url::fromRoute('user.login'));
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current title: Log in');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::setCurrentPageTitle
   */
  private function _testSetCurrentPageTitle(): void {
    $this->drupalGet(Url::fromRoute('druhels_test.change_page_title_in_runtime', ['title' => 'New title']));
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('New title');
  }

  /**
   * @covers ::getCurrentSystemPath
   */
  private function _testGetCurrentSystemPath(): void {
    $web_assert = $this->assertSession();

    // Check on system url
    $node1 = $this->createNode();
    $this->drupalGetEntityPage($node1);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current system path: node/' . $node1->id());

    // Check on url alias
    $node2 = $this->createNode([
      'path' => [
        'alias' => '/test-node',
      ],
    ]);
    $this->drupalGet('/test-node');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current system path: node/' . $node2->id());

    // Clean
    $this->deleteEntities($node1, $node2);
  }

  /**
   * @covers ::getCurrentPathAlias
   */
  private function _testGetCurrentPathAlias(): void {
    $web_assert = $this->assertSession();

    // Check on system url
    $node1 = $this->createNode();
    $this->drupalGetEntityPage($node1);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current path alias: node/' . $node1->id());

    // Check on url alias
    $node2 = $this->createNode([
      'path' => [
        'alias' => '/test-node',
      ],
    ]);
    $this->drupalGet('/test-node');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current path alias: test-node');

    // Clean
    $this->deleteEntities($node1, $node2);
  }

  /**
   * @covers ::getCurrentRouteName
   */
  private function _testGetCurrentRouteName(): void {
    $web_assert = $this->assertSession();

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current route name: entity.node.canonical');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::isAdminRoute
   */
  private function _testIsAdminRoute(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is admin route: 0');

    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/content');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is admin route: 1');

    // Clean
    $this->drupalLogout();
  }

  /**
   * @covers ::isFrontPage
   */
  private function _testIsFrontPage(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet(Url::fromRoute('<front>'));
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is front page: 1');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is front page: 0');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::timerStart
   * @covers ::timerStop
   */
  private function _testTimer(): void {
    DrupalHelper::timerStart();
    sleep(1);
    DrupalHelper::timerStop();
    $status_messages = $this->container->get('messenger')->messagesByType('status');
    $this->assertCount(1, $status_messages);
    $this->assertStringContainsString(' ms', $status_messages[0]);
  }

  /**
   * @covers ::getDefaultLangcode
   */
  private function _testGetDefaultLangcode(): void {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('Default langcode: en');
  }

  /**
   * @covers ::getCurrentLangcode
   */
  private function _testGetCurrentLangcode(): void {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('Current langcode: en');
  }

  /**
   * @covers ::getSiteMail
   */
  private function _testGetSiteMail(): void {
    $this->assertSame('simpletest@example.com', DrupalHelper::getSiteMail());
  }

  /**
   * @covers ::sendMail
   */
  private function _testSendMail(): void {
    DrupalHelper::sendMail('test@example.com', 'Test e-mail', 'E-mail body');
    $this->assertMail('subject', 'Test e-mail');
  }

  /**
   * @covers ::getBreakpointMediaQuery
   */
  private function _testGetBreakpointMediaQuery(): void {
    $this->assertSame('(max-width: 640px)', DrupalHelper::getBreakpointMediaQuery('druhels_test', 'druhels_test.mobile'));
  }

  /**
   * @covers ::render
   */
  private function _testRender(): void {
    $renderer = $this->container->get('renderer'); /** @var RendererInterface $renderer */
    $output = $renderer->executeInRenderContext(new RenderContext(), function () {
      return DrupalHelper::render(['#markup' => 'test']);
    });
    $this->assertSame('test', $output);
  }

}
