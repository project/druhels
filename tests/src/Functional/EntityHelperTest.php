<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\block\Entity\Block;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\druhels\EntityHelper;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * @coversDefaultClass \Drupal\druhels\EntityHelper
 */
class EntityHelperTest extends BrowserTestBase {

  use AssertMailTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
    'block',
    'datetime',
    'node',
    'filter',
    'options',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->placeBlock('system_messages_block', [
      'id' => 'system_messages_block',
    ]);
    $this->placeBlock('system_info_block', [
      'id' => 'system_info',
      'label' => 'System info',
    ]);

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->createField('node', 'page', 'field_string', 'string', 'String');
    $this->createField('node', 'page', 'field_strings', 'string', 'Strings', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED]);
    $this->createField('node', 'page', 'field_list', 'list_integer', 'List', [
      'settings' => [
        'allowed_values' => [
          0 => 'Foo',
          1 => 'Bar',
        ],
      ],
    ]);
    $this->createField('node', 'page', 'field_lists', 'list_integer', 'List (multiple)', [
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'allowed_values' => [
          0 => 'Foo',
          1 => 'Bar',
        ],
      ],
    ]);
    $this->createField('node', 'page', 'field_references', 'entity_reference', 'References', [
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
  }

  /**
   * Tests runner.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::getFieldLabel
   */
  private function _testGetFieldLabel(): void {
    $this->assertSame('String', EntityHelper::getFieldLabel('node', 'page', 'field_string'));
    $this->assertSame('Title', EntityHelper::getFieldLabel('node', 'page', 'title'));
  }

  /**
   * @covers ::getListFieldAllowedValues
   */
  private function _testGetListFieldAllowedValues(): void {
    $this->assertSame([0 => 'Foo', 1 => 'Bar'], EntityHelper::getListFieldAllowedValues('node', 'page', 'field_list'));
    $this->assertSame(NULL, EntityHelper::getListFieldAllowedValues('node', 'page', 'non_exists_field'));
  }

  /**
   * @covers ::getListFieldValueLabel
   */
  private function _testGetListFieldValueLabel(): void {
    $node = $this->createNode(['field_list' => 0]);
    $this->assertSame('Foo', EntityHelper::getListFieldValueLabel($node->get('field_list')));
    $this->assertSame('Foo', EntityHelper::getListFieldValueLabel($node->get('field_list')->first()));

    $node->set('field_list', 1);
    $node->save();
    $this->assertSame('Bar', EntityHelper::getListFieldValueLabel($node->get('field_list')));
    $this->assertSame('Bar', EntityHelper::getListFieldValueLabel($node->get('field_list')->first()));

    $this->assertSame(NULL, EntityHelper::getListFieldValueLabel($node->get('field_lists')));

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getFieldValues
   */
  private function _testGetFieldValues(): void {
    $node1 = $this->createNode();
    $node2 = $this->createNode([
      'field_string' => 'Foo',
      'field_strings' => ['Foo', 'Bar'],
      'field_references' => [$node1->id()],
    ]);
    $this->assertSame(['Foo'], EntityHelper::getFieldValues($node2->get('field_string')));
    $this->assertSame(['Foo'], EntityHelper::getFieldValues($node2->get('field_string'), 'value'));
    $this->assertSame(['Foo', 'Bar'], EntityHelper::getFieldValues($node2->get('field_strings')));
    $this->assertSame(['Foo', 'Bar'], EntityHelper::getFieldValues($node2->get('field_strings'), 'value'));
    $this->assertSame([$node1->id()], EntityHelper::getFieldValues($node2->get('field_references')));
    $this->assertSame([$node1->id()], EntityHelper::getFieldValues($node2->get('field_references'), 'target_id'));

    // Clean
    $this->deleteEntities($node1, $node2);
  }

  /**
   * @covers ::getExistsFieldValue
   */
  private function _testGetExistsFieldValue(): void {
    $node = $this->createNode(['field_string' => 'Foo']);
    $this->assertSame('Foo', EntityHelper::getExistsFieldValue($node, 'field_string'));
    $this->assertSame(NULL, EntityHelper::getExistsFieldValue($node, 'field_non_exists'));
  }

  /**
   * @covers ::getReferencedEntitiesFieldValues
   */
  private function _testGetReferencedEntitiesFieldValues(): void  {
    $child_nodes = [
      1 => $this->createNode([
        'title' => 'Ch1',
        'field_string' => 'Foo',
        'field_strings' => ['Foo1', 'Foo2'],
      ]),
      2 => $this->createNode([
        'title' => 'Ch2',
        'field_string' => 'Bar',
        'field_strings' => ['Bar1', 'Bar2'],
      ]),
      3 => $this->createNode([
        'title' => 'Ch3',
        'field_string' => 'Baz',
      ]),
    ];
    $parent_node = $this->createNode([
      'field_references' => [
        $child_nodes[1]->id(),
        $child_nodes[2]->id(),
      ],
    ]);
    $this->assertSame(['Ch1', 'Ch2'], EntityHelper::getReferencedEntitiesFieldValues($parent_node->get('field_references'), 'title'));
    $this->assertSame(['Foo', 'Bar'], EntityHelper::getReferencedEntitiesFieldValues($parent_node->get('field_references'), 'field_string'));
    $this->assertSame(['Foo1', 'Foo2', 'Bar1', 'Bar2'], EntityHelper::getReferencedEntitiesFieldValues($parent_node->get('field_references'), 'field_strings'));
  }

  /**
   * @covers ::bundleHasField
   */
  private function _testBundleHasField(): void {
    $this->assertSame(TRUE, EntityHelper::bundleHasField('node', 'page', 'field_string'));
    $this->assertSame(FALSE, EntityHelper::bundleHasField('node', 'page', 'field_non_exists'));
  }

  /**
   * @covers ::getBundleFieldDefinitionsByType
   */
  private function _testGetFieldDefinitionsByType(): void {
    $string_fields = EntityHelper::getBundleFieldDefinitionsByType('node', 'page', 'string');
    $this->assertCount(3, $string_fields);
    $this->assertSame('Title', (string)$string_fields['title']->getLabel());
    $this->assertSame('String', (string)$string_fields['field_string']->getLabel());
    $this->assertSame('Strings', (string)$string_fields['field_strings']->getLabel());
  }

  /**
   * @covers ::view
   */
  private function _testView(): void {
    $node = $this->createNode();
    $node_build = EntityHelper::view($node);
    $this->assertArrayHasKey('#theme', $node_build);
    $this->assertSame('node', $node_build['#theme']);

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::viewWithFields
   */
  private function _testViewWithFields(): void {
    $node = $this->createNode();
    $node_build = EntityHelper::viewWithFields($node);
    $this->assertArrayHasKey('#theme', $node_build);
    $this->assertSame('node', $node_build['#theme']);
    $this->assertArrayHasKey('title', $node_build);
    $this->assertSame('field', $node_build['title']['#theme']);

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::deleteMultiple
   */
  private function _testDeleteMultiple(): void {
    $node1 = $this->createNode();
    $node2 = $this->createNode();
    $node1_id = $node1->id();
    $node2_id = $node2->id();
    EntityHelper::deleteMultiple([$node1_id, $node2_id], 'node');
    $this->assertNull(Node::load($node1_id));
    $this->assertNull(Node::load($node2_id));
  }

  /**
   * @covers ::fieldHasAffectingChanges
   */
  private function _testFieldHasAffectingChanges(): void {
    $node = $this->createNode(['field_string' => 'Foo']);
    $node->original = clone $node;
    $this->assertSame(FALSE, EntityHelper::fieldHasAffectingChanges($node, 'field_string'));
    $node->set('field_string', 'Bar');
    $node->save();
    // @TODO Fix
    //$this->assertSame(TRUE, EntityHelper::fieldHasAffectingChanges($node, 'field_string'));

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::fieldHasValue
   */
  private function _testFieldHasValue(): void {
    $node = $this->createNode(['field_list' => 0]);
    $this->assertSame(TRUE, EntityHelper::fieldHasValue($node->get('field_list'), 0));
    $this->assertSame(FALSE, EntityHelper::fieldHasValue($node->get('field_list'), 1));

    $node->set('field_list', 1);
    $node->save();
    $this->assertSame(FALSE, EntityHelper::fieldHasValue($node->get('field_list'), 0));
    $this->assertSame(TRUE, EntityHelper::fieldHasValue($node->get('field_list'), 1));

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::referencedEntitiesFieldHasValue
   */
  private function _testReferencedEntitiesFieldHasValue(): void {
    $child_node1 = $this->createNode(['field_strings' => ['foo', 'bar']]);
    $child_node2 = $this->createNode(['field_strings' => ['bar']]);
    $parent_node = $this->createNode(['field_references' => [$child_node1->id(), $child_node2->id()]]);

    $this->assertSame(TRUE, EntityHelper::referencedEntitiesFieldHasValue($parent_node->get('field_references'), 'field_strings', 'foo'));
    $this->assertSame(TRUE, EntityHelper::referencedEntitiesFieldHasValue($parent_node->get('field_references'), 'field_strings', 'bar'));
    $this->assertSame(FALSE, EntityHelper::referencedEntitiesFieldHasValue($parent_node->get('field_references'), 'field_strings', 'baz'));

    // Clean
    $this->deleteEntities($child_node1, $child_node2, $parent_node);
  }

  /**
   * @covers ::getContentEntityTypes
   */
  private function _testGetContentEntityTypes(): void {
    $content_entity_types = EntityHelper::getContentEntityTypes();
    $this->assertTrue(TRUE, count($content_entity_types) > 0);
    $this->assertArrayHasKey('node', $content_entity_types);
    $this->assertSame('Content', (string)$content_entity_types['node']->getLabel());
    $this->assertArrayNotHasKey('block', $content_entity_types);
  }

  /**
   * @covers ::isEntityPage
   */
  private function _testIsEntityPage(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user/login');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is entity page: 0');
    $web_assert->pageTextContains('Is entity node page: 0');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is entity page: 1');
    $web_assert->pageTextContains('Is entity node page: 1');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getCurrentEntity
   */
  private function _testGetCurrentEntity(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user/login');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current entity id: ~');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current entity id: ' . $node->id());

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::setThirdPartySettings
   */
  private function _testSetThirdPartySettings(): void {
    $block = Block::load('system_info');
    EntityHelper::setThirdPartySettings($block, 'druhels', ['foo' => 'bar']);
    $this->assertSame('bar', $block->getThirdPartySetting('druhels', 'foo'));
  }

}
