<?php

namespace Drupal\Tests\druhels\Functional;

use Drupal\druhels\ArrayHelper;
use Drupal\druhels\TaxonomyHelper;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * @coversDefaultClass \Drupal\druhels\TaxonomyHelper
 */
class TaxonomyHelperTest extends BrowserTestBase {

  use ImprovementsTestTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'druhels',
    'druhels_test',
    'block',
    'taxonomy',
    'node',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  protected VocabularyInterface $vocabulary;

  protected array $terms;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->placeBlock('system_messages_block', [
      'id' => 'system_messages_block',
    ]);
    $this->placeBlock('system_info_block', [
      'id' => 'system_info',
      'label' => 'System info',
    ]);

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->vocabulary = $this->createVocabulary();

    $this->terms['1']     = $this->createTerm($this->vocabulary, ['name' => 'Term 1',     'parent' => 0,                         'weight' => 1]);
    $this->terms['1.1']   = $this->createTerm($this->vocabulary, ['name' => 'Term 1.1',   'parent' => $this->terms['1']->id(),   'weight' => 2]);
    $this->terms['1.2']   = $this->createTerm($this->vocabulary, ['name' => 'Term 1.2',   'parent' => $this->terms['1']->id(),   'weight' => 3]);
    $this->terms['1.2.1'] = $this->createTerm($this->vocabulary, ['name' => 'Term 1.2.1', 'parent' => $this->terms['1.2']->id(), 'weight' => 4]);
    $this->terms['2']     = $this->createTerm($this->vocabulary, ['name' => 'Term 2',     'parent' => 0,                         'weight' => 5]);
    $this->terms['2.1']   = $this->createTerm($this->vocabulary, ['name' => 'Term 2.1',   'parent' => $this->terms['2']->id(),   'weight' => 6]);
  }

  /**
   * Tests runner.
   */
  public function testMain(): void {
    $this->runAllPrivateTests();
  }

  /**
   * @covers ::getTermByName
   */
  private function _testGetTermByName(): void {
    $vid = $this->vocabulary->id();
    $this->assertSame($this->terms['1']->id(),     TaxonomyHelper::getTermByName($vid, $this->terms['1']->label())->id());
    $this->assertSame($this->terms['1']->id(),     TaxonomyHelper::getTermByName($vid, $this->terms['1']->label(), 0)->id());
    $this->assertSame($this->terms['1.2.1']->id(), TaxonomyHelper::getTermByName($vid, $this->terms['1.2.1']->label(), $this->terms['1.2']->id())->id());
    $this->assertSame(NULL,                        TaxonomyHelper::getTermByName($vid, 'Non exist'));
  }

  /**
   * @covers ::createTerm
   */
  private function _testCreateTerm(): void {
    $vid = $this->vocabulary->id();

    // Create term
    $term = TaxonomyHelper::createTerm($vid, 'Test term');
    $term_id = $term->id();
    $this->assertInstanceOf(TermInterface::class, $term);

    // Term already created
    $term = TaxonomyHelper::createTerm($vid, 'Test term');
    $this->assertSame($term_id, $term->id());

    // Create child term with non-unique name
    $term2 = TaxonomyHelper::createTerm($vid, 'Test term', $term_id);
    $this->assertNotEquals($term_id, $term2->id());
    $this->assertSame($term_id, $term2->get('parent')->entity->id());

    // Create term without name
    $term3 = TaxonomyHelper::createTerm($vid, '');
    $this->assertSame(NULL, $term3);

    // Clean
    $this->deleteEntities($term2, $term);
  }

  /**
   * @covers ::createTerms
   */
  private function _testCreateTerms(): void {
    $vid = $this->vocabulary->id();

    // Create terms
    $terms_name = [$this->randomString(), $this->randomString()];
    $terms1 = TaxonomyHelper::createTerms($vid, $terms_name);
    foreach (array_values($terms1) as $index => $term) {
      $this->assertSame($terms_name[$index], $term->label());
    }

    // Term already created
    $terms2 = TaxonomyHelper::createTerms($vid, $terms_name);
    $this->assertSame(array_keys($terms1), array_keys($terms2));

    // Clean
    $this->deleteEntities(...$terms1);
  }

  /**
   * @covers ::getTermDepth
   */
  private function _testGetTermDepth(): void {
    $this->assertSame(0, TaxonomyHelper::getTermDepth($this->terms['1']));
    $this->assertSame(0, TaxonomyHelper::getTermDepth($this->terms['1']->id()));
    $this->assertSame(1, TaxonomyHelper::getTermDepth($this->terms['1.1']));
    $this->assertSame(1, TaxonomyHelper::getTermDepth($this->terms['1.2']));
    $this->assertSame(2, TaxonomyHelper::getTermDepth($this->terms['1.2.1']));
    $this->assertSame(NULL, TaxonomyHelper::getTermDepth(99999));
  }

  /**
   * @covers ::getTermsByNamesHierarchy
   */
  private function _testGetTermsByNamesHierarchy(): void {
    $vid = $this->vocabulary->id();

    // Term 1
    $result = TaxonomyHelper::getTermsByNamesHierarchy($vid, ['Term 1']);
    $this->assertCount(1, $result);
    $this->assertArrayHasKey($this->terms['1']->id(), $result);
    $this->assertSame($this->terms['1']->label(), $result[$this->terms['1']->id()]->label());

    // Term 1.2
    $result = TaxonomyHelper::getTermsByNamesHierarchy($vid, ['Term 2']);
    $this->assertCount(1, $result);
    $this->assertArrayHasKey($this->terms['2']->id(), $result);
    $this->assertSame($this->terms['2']->label(), $result[$this->terms['2']->id()]->label());

    // Term 1.2.1
    $result = TaxonomyHelper::getTermsByNamesHierarchy($vid, ['Term 1', 'Term 1.2', 'Term 1.2.1']);
    $this->assertCount(3, $result);
    $this->assertArrayHasKey($this->terms['1']->id(), $result);
    $this->assertArrayHasKey($this->terms['1.2']->id(), $result);
    $this->assertArrayHasKey($this->terms['1.2.1']->id(), $result);
    $this->assertSame($this->terms['1']->label(), $result[$this->terms['1']->id()]->label());
    $this->assertSame($this->terms['1.2']->label(), $result[$this->terms['1.2']->id()]->label());

    // Bad hierarchy
    $this->assertSame([], TaxonomyHelper::getTermsByNamesHierarchy($vid, ['Term 1.2']));
    $this->assertSame([], TaxonomyHelper::getTermsByNamesHierarchy($vid, ['Term 1', 'Non exist term name']));
  }

  /**
   * @covers ::createTermsByNamesHierarchy
   */
  private function _testCreateTermsByNamesHierarchy(): void {
    $vid = $this->vocabulary->id();

    $terms_count = $this->getTermsCount($vid);
    $terms = TaxonomyHelper::createTermsByNamesHierarchy($vid, ['Cat 1', 'Cat 1.1', 'Cat 1.1.1']);
    $this->assertCount(3, $terms);
    $this->assertSame($terms_count + 3, $this->getTermsCount($vid));
    $this->assertSame('Cat 1',     ArrayHelper::getValueByIndex($terms, 0)->label());
    $this->assertSame('Cat 1.1',   ArrayHelper::getValueByIndex($terms, 1)->label());
    $this->assertSame('Cat 1.1.1', ArrayHelper::getValueByIndex($terms, 2)->label());

    // Already created
    $terms_count = $this->getTermsCount($vid);
    $terms2 = TaxonomyHelper::createTermsByNamesHierarchy($vid, ['Cat 1', 'Cat 1.1', 'Cat 1.1.1']);
    $this->assertSame(array_keys($terms), array_keys($terms2));
    $this->assertSame($terms_count, $this->getTermsCount($vid));

    // Empty term name
    $terms_count = $this->getTermsCount($vid);
    $terms3 = TaxonomyHelper::createTermsByNamesHierarchy($vid, ['Cat 1', 'Cat 1.1', '']);
    $this->assertCount(2, $terms3);
    $this->assertSame($terms_count, $this->getTermsCount($vid));

    // Create one deepest term
    $terms_count = $this->getTermsCount($vid);
    $terms4 = TaxonomyHelper::createTermsByNamesHierarchy($vid, ['Cat 1', 'Cat 1.1', 'Cat 1.1.2']);
    $this->assertCount(3, $terms4);
    $this->assertSame($terms_count + 1, $this->getTermsCount($vid));

    // Clean
    $this->deleteEntities(...$terms);
    $this->deleteEntities(end($terms4));
  }

  /**
   * @covers ::getTermsByVocabulary
   */
  private function _testGetVocabularyTerms(): void {
    // All terms
    $result = TaxonomyHelper::getTermsByVocabulary($this->vocabulary->id());
    $this->assertCount(6, $result);
    $this->assertSame($this->terms['1']->label(), $result[$this->terms['1']->id()]->name);
    $this->assertSame($this->terms['2']->label(), $result[$this->terms['2']->id()]->name);

    // All childs
    $result = TaxonomyHelper::getTermsByVocabulary($this->vocabulary->id(), $this->terms['1']->id());
    $this->assertCount(3, $result);
    $this->assertSame($this->terms['1.1']->label(), $result[$this->terms['1.1']->id()]->name);

    // Root terms
    $result = TaxonomyHelper::getTermsByVocabulary($this->vocabulary->id(), 0, 1);
    $this->assertCount(2, $result);
    $this->assertSame($this->terms['1']->label(), $result[$this->terms['1']->id()]->name);
    $this->assertSame($this->terms['2']->label(), $result[$this->terms['2']->id()]->name);

    // Max depth 2
    $result = TaxonomyHelper::getTermsByVocabulary($this->vocabulary->id(), 0, 2);
    $this->assertCount(5, $result);

    // Root terms + Load entities
    $result = TaxonomyHelper::getTermsByVocabulary($this->vocabulary->id(), 0, 1, TRUE);
    $this->assertCount(2, $result);
    $this->assertSame($this->terms['1']->label(), $result[$this->terms['1']->id()]->label());
    $this->assertSame($this->terms['2']->label(), $result[$this->terms['2']->id()]->label());

    // Non exists vocabulary terms
    $this->assertSame([], TaxonomyHelper::getTermsByVocabulary('non_exists_vocabulary'));
  }

  /**
   * @covers ::getAllChildTerms
   */
  private function _testGetAllChildTerms(): void {
    $vid = $this->vocabulary->id();

    // Term 1 childs
    $result = TaxonomyHelper::getAllChildTerms($vid, $this->terms['1']->id());
    $this->assertCount(3, $result);
    $this->assertSame($this->terms['1.1']->id(),   $result[$this->terms['1.1']->id()]->tid);
    $this->assertSame($this->terms['1.2']->id(),   $result[$this->terms['1.2']->id()]->tid);
    $this->assertSame($this->terms['1.2.1']->id(), $result[$this->terms['1.2.1']->id()]->tid);

    // Term 2.1 childs
    $this->assertCount(0, TaxonomyHelper::getAllChildTerms($vid, $this->terms['2.1']->id()));

    // Term 1.2 childs with entity load
    $result = TaxonomyHelper::getAllChildTerms($vid, $this->terms['1.2']->id(), TRUE);
    $this->assertCount(1, $result);
    $this->assertSame($this->terms['1.2.1']->id(), $result[$this->terms['1.2.1']->id()]->id());

    // Non exists term childs
    $this->assertSame([], TaxonomyHelper::getAllChildTerms($vid, 99999));
  }

  /**
   * @covers ::getAllChildTermsIds
   */
  private function _testGetAllChildTermsIds(): void {
    $vid = $this->vocabulary->id();

    // Term 1 childs
    $this->assertSame([
      (int)$this->terms['1.1']->id(),
      (int)$this->terms['1.2']->id(),
      (int)$this->terms['1.2.1']->id()
    ], TaxonomyHelper::getAllChildTermsIds($vid, $this->terms['1']->id()));

    // Term 1 childs with self
    $this->assertSame([
      (int)$this->terms['1']->id(),
      (int)$this->terms['1.1']->id(),
      (int)$this->terms['1.2']->id(),
      (int)$this->terms['1.2.1']->id()
    ], TaxonomyHelper::getAllChildTermsIds($vid, $this->terms['1']->id(), TRUE));

    // Term 1.2 childs
    $this->assertSame([(int)$this->terms['1.2.1']->id()], TaxonomyHelper::getAllChildTermsIds($vid, $this->terms['1.2']->id()));

    // Non exsist term childs
    $this->assertSame([], TaxonomyHelper::getAllChildTermsIds($vid, 999999));
  }

  /**
   * @covers ::getDirectChildTerms
   */
  private function _testGetDirectChildTerms(): void {
    $vid = $this->vocabulary->id();

    // Term 1 direct child
    $result = TaxonomyHelper::getDirectChildTerms($vid, $this->terms['1']->id());
    $this->assertCount(2, $result);
    $this->assertSame($this->terms['1.1']->id(), $result[$this->terms['1.1']->id()]->tid);
    $this->assertSame($this->terms['1.2']->id(), $result[$this->terms['1.2']->id()]->tid);

    // Term 1.2 direct child
    $result = TaxonomyHelper::getDirectChildTerms($vid, $this->terms['1.2']->id());
    $this->assertCount(1, $result);
    $this->assertSame($this->terms['1.2.1']->id(), $result[$this->terms['1.2.1']->id()]->tid);

    // Non exists term childs
    $this->assertSame([], TaxonomyHelper::getDirectChildTerms($vid, 99999));
  }

  /**
   * @covers ::getDirectChildTermsIds
   */
  private function _testGetDirectChildTermsIds(): void {
    // Root terms
    $this->assertSame([
      (int)$this->terms['1']->id(),
      (int)$this->terms['2']->id(),
    ], TaxonomyHelper::getDirectChildTermsIds($this->vocabulary->id(), 0));

    // Term 1 childs
    $this->assertSame([
      (int)$this->terms['1.1']->id(),
      (int)$this->terms['1.2']->id(),
    ], TaxonomyHelper::getDirectChildTermsIds($this->vocabulary->id(), $this->terms['1']->id()));

    // Term 1 childs with self
    $this->assertSame([
      (int)$this->terms['1']->id(),
      (int)$this->terms['1.1']->id(),
      (int)$this->terms['1.2']->id(),
    ], TaxonomyHelper::getDirectChildTermsIds($this->vocabulary->id(), $this->terms['1']->id(), TRUE));

    // Non exists term childs
    $this->assertSame([], TaxonomyHelper::getDirectChildTermsIds($this->vocabulary->id(), 99999));
  }

  /**
   * @covers ::termHasChilds
   */
  private function _testTermHasChilds(): void {
    $this->assertSame(TRUE,  TaxonomyHelper::termHasChilds($this->terms['1']->id()));
    $this->assertSame(FALSE, TaxonomyHelper::termHasChilds($this->terms['1.1']->id()));
    $this->assertSame(TRUE,  TaxonomyHelper::termHasChilds($this->terms['1.2']->id()));
    $this->assertSame(TRUE,  TaxonomyHelper::termHasChilds($this->terms['2']->id()));
    $this->assertSame(FALSE, TaxonomyHelper::termHasChilds(999999));
  }

  /**
   * @covers ::getTermParent
   */
  private function _testGetTermParent(): void {
    $this->assertSame(NULL,                      TaxonomyHelper::getTermParent($this->terms['1']));
    $this->assertSame($this->terms['1']->id(),   TaxonomyHelper::getTermParent($this->terms['1.1'])->id());
    $this->assertSame($this->terms['1']->id(),   TaxonomyHelper::getTermParent($this->terms['1.2'])->id());
    $this->assertSame($this->terms['1.2']->id(), TaxonomyHelper::getTermParent($this->terms['1.2.1'])->id());
    $this->assertSame($this->terms['2']->id(),   TaxonomyHelper::getTermParent($this->terms['2.1'])->id());
    $this->assertSame(NULL,                      TaxonomyHelper::getTermParent(99999));
  }

  /**
   * @covers ::getTermParentId
   */
  private function _testGetTermParentId(): void {
    $this->assertSame(NULL,                           TaxonomyHelper::getTermParentId($this->terms['1']));
    $this->assertSame((int)$this->terms['1']->id(),   TaxonomyHelper::getTermParentId($this->terms['1.1']));
    $this->assertSame((int)$this->terms['1']->id(),   TaxonomyHelper::getTermParentId($this->terms['1.2']));
    $this->assertSame((int)$this->terms['1.2']->id(), TaxonomyHelper::getTermParentId($this->terms['1.2.1']));
    $this->assertSame((int)$this->terms['2']->id(),   TaxonomyHelper::getTermParentId($this->terms['2.1']));
    $this->assertSame(NULL,                           TaxonomyHelper::getTermParentId(99999));
  }

  /**
   * @covers ::getAllParentsTerms
   */
  private function _testGetAllParentsTerms(): void {
    // Term 1 parents without self
    $this->assertSame([], TaxonomyHelper::getAllParentsTerms($this->terms['1']->id(), FALSE));

    // Term 1 parents with self
    $result = TaxonomyHelper::getAllParentsTerms($this->terms['1']->id());
    $this->assertCount(1, $result);
    $this->assertSame($this->terms['1']->id(), $result[$this->terms['1']->id()]->id());

    // Term 1.2 parents with self
    $result = TaxonomyHelper::getAllParentsTerms($this->terms['1.2']->id());
    $this->assertCount(2, $result);
    $this->assertSame($this->terms['1']->id(), ArrayHelper::getValueByIndex($result, 0)->id());
    $this->assertSame($this->terms['1.2']->id(), ArrayHelper::getValueByIndex($result, 1)->id());

    // Non exists term parents
    $this->assertSame([], TaxonomyHelper::getAllParentsTerms(999999));
  }

  /**
   * @covers ::getAllParentsTermsRaw
   */
  private function _testGetAllParentsTermsRaw(): void {
    $vid = $this->vocabulary->id();

    // Term 1 parents without self
    $this->assertSame([], TaxonomyHelper::getAllParentsTermsRaw($this->terms['1']->id(), $vid, FALSE));

    // Term 1 parent with self
    $result = TaxonomyHelper::getAllParentsTermsRaw($this->terms['1']->id(), $vid);
    $this->assertCount(1, $result);
    $this->assertSame($this->terms['1']->id(), $result[$this->terms['1']->id()]->tid);

    // Term 1.2 parents with self
    $result = TaxonomyHelper::getAllParentsTermsRaw($this->terms['1.2']->id(), $vid);
    $this->assertCount(2, $result);
    $this->assertSame($this->terms['1']->id(), ArrayHelper::getValueByIndex($result, 0)->tid);
    $this->assertSame($this->terms['1.2']->id(), ArrayHelper::getValueByIndex($result, 1)->tid);

    // Non exists term parents
    $this->assertSame([], TaxonomyHelper::getAllParentsTermsRaw(999999, $vid));
  }

  /**
   * @covers ::getAllParentsTermsIds
   */
  private function _testGetAllParentsTermsIds(): void {
    $vid = $this->vocabulary->id();

    // Term 1 parents without self
    $this->assertSame([], TaxonomyHelper::getAllParentsTermsIds($this->terms['1']->id(), $vid, FALSE));

    // Term 1 parents with self
    $result = TaxonomyHelper::getAllParentsTermsIds($this->terms['1']->id(), $vid);
    $this->assertCount(1, $result);
    $this->assertSame([(int)$this->terms['1']->id()], $result);

    // Term 1.2 parents with self
    $result = TaxonomyHelper::getAllParentsTermsIds($this->terms['1.2']->id(), $vid);
    $this->assertSame([
      (int)$this->terms['1']->id(),
      (int)$this->terms['1.2']->id(),
    ], $result);

    // Non exists term parents
    $this->assertSame([999999], TaxonomyHelper::getAllParentsTermsIds(999999, $vid));
  }

  /**
   * @covers ::getAllParentsTermsNameRaw
   */
  private function _testGetAllParentsTermsNameRaw(): void {
    $vid = $this->vocabulary->id();

    $this->assertSame([
      (int)$this->terms['1']->id() => $this->terms['1']->label()
    ], TaxonomyHelper::getAllParentsTermsNameRaw($this->terms['1']->id(), $vid));

    $this->assertSame([], TaxonomyHelper::getAllParentsTermsNameRaw($this->terms['1']->id(), $vid, FALSE));

    $this->assertSame([
      (int)$this->terms['1']->id()   => $this->terms['1']->label(),
      (int)$this->terms['1.2']->id() => $this->terms['1.2']->label(),
    ], TaxonomyHelper::getAllParentsTermsNameRaw($this->terms['1.2']->id(), $vid));

    $this->assertSame([], TaxonomyHelper::getAllParentsTermsNameRaw(99999, $vid));
    $this->assertSame([], TaxonomyHelper::getAllParentsTermsNameRaw(99999, $vid, FALSE));
  }

  /**
   * @covers ::getDirectParentsIds
   */
  private function _testGetDirectParentsIds(): void {
    $this->assertSame([], TaxonomyHelper::getDirectParentsIds($this->terms['1']));
    $this->assertSame([], TaxonomyHelper::getDirectParentsIds($this->terms['1']->id()));
    $this->assertSame([(int)$this->terms['1']->id()], TaxonomyHelper::getDirectParentsIds($this->terms['1.1']->id()));
    $this->assertSame([(int)$this->terms['1']->id()], TaxonomyHelper::getDirectParentsIds($this->terms['1.2']->id()));
  }

  /**
   * @covers ::getRootParentTerm
   */
  private function _testGetRootParentTerm(): void {
    $this->assertSame($this->terms['1']->id(), TaxonomyHelper::getRootParentTerm($this->terms['1']->id())->id());
    $this->assertSame($this->terms['1']->id(), TaxonomyHelper::getRootParentTerm($this->terms['1.1']->id())->id());
    $this->assertSame($this->terms['1']->id(), TaxonomyHelper::getRootParentTerm($this->terms['1.2']->id())->id());
    $this->assertSame($this->terms['1']->id(), TaxonomyHelper::getRootParentTerm($this->terms['1.2.1']->id())->id());
    $this->assertSame($this->terms['2']->id(), TaxonomyHelper::getRootParentTerm($this->terms['2.1']->id())->id());
    $this->assertSame(NULL,                    TaxonomyHelper::getRootParentTerm(99999));
  }

  /**
   * @covers ::getRootParentTermId
   */
  private function _testGetRootParentTermId(): void {
    $this->assertSame((int)$this->terms['1']->id(), TaxonomyHelper::getRootParentTermId($this->terms['1']->id()));
    $this->assertSame((int)$this->terms['1']->id(), TaxonomyHelper::getRootParentTermId($this->terms['1.1']->id()));
    $this->assertSame((int)$this->terms['1']->id(), TaxonomyHelper::getRootParentTermId($this->terms['1.2']->id()));
    $this->assertSame((int)$this->terms['1']->id(), TaxonomyHelper::getRootParentTermId($this->terms['1.2.1']->id()));
    $this->assertSame((int)$this->terms['2']->id(), TaxonomyHelper::getRootParentTermId($this->terms['2.1']->id()));
    $this->assertSame(NULL,                         TaxonomyHelper::getRootParentTermId(99999));
  }

  /**
   * @covers ::isTermPage
   */
  private function _testIsTermPage(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is term page: 0');

    $this->drupalGetEntityPage($this->terms['1']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is term page: 1');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Is term page: 0');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getCurrentTermId
   */
  private function _testGetCurrentTermId(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term id: ~');

    $this->drupalGetEntityPage($this->terms['1']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term id: ' . $this->terms['1']->id());

    $this->drupalGet('/taxonomy/term/all');
    $this->dontSeeErrorMessage(FALSE);
    $web_assert->pageTextContains('Current term id: ~');

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term id: ~');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getCurrentTerm
   */
  private function _testGetCurrentTerm(): void {
    $web_assert = $this->assertSession();

    $this->drupalGet('/user');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term: ~');

    $this->drupalGetEntityPage($this->terms['1']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term: ' . $this->terms['1']->id());

    $node = $this->createNode();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Current term: ~');

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * @covers ::getTermIdByProperties
   */
  private function _testGetTermIdByProperties(): void {
    $this->assertSame((int)$this->terms['1']->id(), TaxonomyHelper::getTermIdByProperties(['name' => 'Term 1']));
    $this->assertSame((int)$this->terms['2']->id(), TaxonomyHelper::getTermIdByProperties(['name' => 'Term 2', 'vid' => $this->vocabulary->id()]));
    $this->assertSame(NULL,                         TaxonomyHelper::getTermIdByProperties(['name' => 'Non exists term name']));
  }

  /**
   * @covers ::getTermUrl
   */
  private function _testGetTermUrl(): void {
    $url = TaxonomyHelper::getTermUrl($this->terms['1']->id());
    $this->assertSame($this->terms['1']->toUrl()->toString(), $url->toString());

    $this->assertSame('/taxonomy/term/99999', TaxonomyHelper::getTermUrl(99999)->toString());
  }

  /**
   * @covers ::getTermStorage
   */
  private function _testGetTermStorage(): void {
    $this->assertInstanceOf(TermStorageInterface::class, TaxonomyHelper::getTermStorage());
  }

  /**
   * @covers ::getTermsName
   */
  private function _testGetTermsName(): void {
    $term1_id = $this->terms['1']->id();
    $term2_id = $this->terms['2']->id();

    $result = TaxonomyHelper::getTermsName([$term1_id, $term2_id]);
    $this->assertSame([
      $term1_id => $this->terms['1']->label(),
      $term2_id => $this->terms['2']->label(),
    ], $result);

    // Test static cache
    $result = TaxonomyHelper::getTermsName([$term1_id, $term2_id]);
    $this->assertSame([
      $term1_id => $this->terms['1']->label(),
      $term2_id => $this->terms['2']->label(),
    ], $result);

    $result = TaxonomyHelper::getTermsName([$term1_id, $term2_id], FALSE);
    $this->assertSame([
      $term1_id => $this->terms['1']->label(),
      $term2_id => $this->terms['2']->label(),
    ], $result);

    // Test static cache
    $result = TaxonomyHelper::getTermsName([$term1_id, $term2_id], FALSE);
    $this->assertSame([
      $term1_id => $this->terms['1']->label(),
      $term2_id => $this->terms['2']->label(),
    ], $result);

    $this->assertSame([], TaxonomyHelper::getTermsName([99998, 99999]));
    $this->assertSame([], TaxonomyHelper::getTermsName([]));
  }

  /**
   * Return terms count.
   */
  private function getTermsCount(string $vocabulary_machine_name): int {
    return \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vocabulary_machine_name)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

}
