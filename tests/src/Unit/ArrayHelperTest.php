<?php

namespace Drupal\Tests\druhels\Unit;

use Drupal\druhels\ArrayHelper;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Drupal\druhels\ArrayHelper
 */
class ArrayHelperTest extends TestCase {

  /**
   * @covers ::removeEmptyElements
   */
  public function testRemoveEmptyElements(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => '',
      0 => 2,
      2 => NULL,
      3 => 0,
      'baz' => 'Baz',
      'qwe' => FALSE,
      'wer' => [],
    ];
    $result = [
      'foo' => 'Foo',
      0 => 2,
      'baz' => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::removeEmptyElements($array));

    $array = [
      0 => 'Foo',
      1 => '',
      2 => 2,
    ];
    $result = [
      0 => 'Foo',
      2 => 2,
    ];
    $this->assertSame($result, ArrayHelper::removeEmptyElements($array));

    $array = [
      'foo' => 'Foo',
      'bar' => '',
      'baz' => [],
      'qwe' => [
        1 => 'One',
        2 => '',
      ],
      'wer' => [
        1 => '',
        2 => '',
      ],
    ];
    $result = [
      'foo' => 'Foo',
      'qwe' => [
        1 => 'One',
      ],
    ];
    $this->assertSame($result, ArrayHelper::removeEmptyElements($array, TRUE));
  }

  /**
   * @covers ::renameKeysToColumnValue
   */
  public function testRenameKeysToColumnValue(): void {
    $array = [
      0 => [
        'id' => '1',
        'title' => 'First',
      ],
      1 => [
        'id' => '3',
        'title' => 'Second',
      ],
    ];
    $result = [
      '1' => [
        'id' => '1',
        'title' => 'First',
      ],
      '3' => [
        'id' => '3',
        'title' => 'Second',
      ],
    ];
    $this->assertSame($result, ArrayHelper::renameKeysToColumnValue($array, 'id'));
  }

  /**
   * @covers ::insertBefore
   */
  public function testInsertBefore(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $result =  [
      'foo' => 'Foo',
      'qwe' => 'Qwe',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 'bar', ['qwe' => 'Qwe']));

    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $result =  [
      'qwe' => 'Qwe',
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 'foo', ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      'qwe' => 'Qwe',
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 0, ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      0 => 'Foo',
      1 => 'Bar',
      'qwe' => 'Qwe',
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 2, ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      3 => 'Qwe',
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 0, [3 => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      2 => 'Qwe',
      0 => 'Foo',
      1 => 'Bar',
    ];
    $this->assertSame($result, ArrayHelper::insertBefore($array, 0, [2 => 'Qwe']));
  }

  /**
   * @covers ::insertAfter
   */
  public function testInsertAfter(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $result =  [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'qwe' => 'Qwe',
      'baz' => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 'bar', ['qwe' => 'Qwe']));

    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $result =  [
      'foo' => 'Foo',
      'qwe' => 'Qwe',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 'foo', ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      0 => 'Foo',
      'qwe' => 'Qwe',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 0, ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
      'qwe' => 'Qwe',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 2, ['qwe' => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      0 => 'Foo',
      3 => 'Qwe',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 0, [3 => 'Qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result =  [
      0 => 'Foo',
      2 => 'Qwe',
      1 => 'Bar',
    ];
    $this->assertSame($result, ArrayHelper::insertAfter($array, 0, [2 => 'Qwe']));
  }

  /**
   * @covers ::insertAfterValue
   */
  public function testInsertAfterValue(): void {
    $array = [
      'foo' => 'Foo item',
      'baz' => 'Baz item',
    ];
    $result = [
      'foo' => 'Foo item',
      'bar' => 'Bar item',
      'baz' => 'Baz item',
    ];
    $this->assertSame($result, ArrayHelper::insertAfterValue($array, 'Foo item', ['bar' => 'Bar item']));

    $array = [
      'foo' => 'Foo item',
      'baz' => 'Baz item',
    ];
    $result = [
      'foo' => 'Foo item',
      'baz' => 'Baz item',
      'bar' => 'Bar item',
    ];
    $this->assertSame($result, ArrayHelper::insertAfterValue($array, 'Baz item', ['bar' => 'Bar item']));

    $array = [
      0 => 'Foo item',
      1 => 'Baz item',
    ];
    $result = [
      0 => 'Foo item',
      'bar' => 'Bar item',
      1 => 'Baz item',
    ];
    $this->assertSame($result, ArrayHelper::insertAfterValue($array, 'Foo item', ['bar' => 'Bar item']));

    $array = [
      1 => 'Foo item',
      3 => 'Baz item',
    ];
    $result = [
      1 => 'Foo item',
      0 => 'Bar item',
      3 => 'Baz item',
    ];
    $this->assertSame($result, ArrayHelper::insertAfterValue($array, 'Foo item', [0 => 'Bar item']));

    $array = [
      1 => 'Foo item',
      2 => 'Bar item',
    ];
    $result = [
      1 => 'Foo item',
      2 => 'Bar item',
      0 => 'Baz item',
    ];
    $this->assertSame($result, ArrayHelper::insertAfterValue($array, 'Non exist value', [0 => 'Baz item']));
  }

  /**
   * @covers ::moveBefore
   */
  public function testMoveBefore(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
      1 => 'One',
    ];
    $result = [
      'foo' => 'Foo',
      'baz' => 'Baz',
      'bar' => 'Bar',
      1 => 'One',
    ];
    $this->assertSame($result, ArrayHelper::moveBefore($array, 'baz', 'bar'));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result = [
      0 => 'Foo',
      2 => 'Baz',
      1 => 'Bar',
    ];
    $this->assertSame($result, ArrayHelper::moveBefore($array, 2, 1));

    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $this->assertSame($array, ArrayHelper::moveBefore($array, 'qwe1', 'qwe2'));
  }

  /**
   * @covers ::renameKey
   */
  public function testRenameKey(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      1 => 'Baz',
    ];
    $result = [
      'foo' => 'Foo',
      'baz' => 'Bar',
      1 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::renameKey($array, 'bar', 'baz'));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result = [
      0 => 'Foo',
      1 => 'Bar',
      3 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::renameKey($array, 2, 3));

    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
    ];
    $this->assertSame($array, ArrayHelper::renameKey($array, 'qwe', 'qqq'));
  }

  /**
   * @covers ::removeElementsByKeys
   */
  public function testRemoveElementsByKey(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $result = [
      'bar' => 'Bar',
    ];
    $this->assertSame($result, ArrayHelper::removeElementsByKeys($array, ['foo', 'baz']));

    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];
    $this->assertSame($array, ArrayHelper::removeElementsByKeys($array, ['qwe']));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
      2 => 'Baz',
    ];
    $result = [
      2 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::removeElementsByKeys($array, [0, 1]));
  }

  /**
   * @covers ::removeExtraElements
   */
  public function testRemoveExtraElements(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'bar',
      'baz' => 'baz',
    ];
    $result = [
      'bar' => 'bar',
      'baz' => 'baz',
    ];
    $this->assertSame($result, ArrayHelper::removeExtraElements($array, ['bar', 'baz', 'qwe']));
  }

  /**
   * @covers ::removeElementFromNestedArray
   */
  public function testRemoveNestedArrayElement(): void {
    $array = [
      'foo' => [
        'bar' => [
          'baz' => 123,
        ],
      ],
      'qwe' => [],
      'wer' => 234,
    ];

    $result = [
      'foo' => [
        'bar' => [],
      ],
      'qwe' => [],
      'wer' => 234,
    ];
    $this->assertSame($result, ArrayHelper::removeElementFromNestedArray($array, ['foo', 'bar', 'baz'], FALSE));

    $result = [
      'qwe' => [],
      'wer' => 234,
    ];
    $this->assertSame($result, ArrayHelper::removeElementFromNestedArray($array, ['foo', 'bar', 'baz'], TRUE));
  }

  /**
   * @coves ::getTextLines
   */
  public function testGetTextLines(): void {
    $text = "Foo\nBar \n\n Baz\n \n";
    $result = [
      0 => 'Foo',
      1 => 'Bar',
      3 => 'Baz',
    ];
    $this->assertSame($result, ArrayHelper::getTextLines($text));

    $this->assertSame([], ArrayHelper::getTextLines(''));
    $this->assertSame([], ArrayHelper::getTextLines(' '));
    $this->assertSame([], ArrayHelper::getTextLines('0'));
    $this->assertSame([0 => '0'], ArrayHelper::getTextLines(0, FALSE));
  }

  /**
   * @covers ::searchInTwodimArray
   */
  public function testSearchInTwodimArray(): void {
    $array = [
      0 => ['id' => 1, 'title' => 'First'],
      1 => ['id' => 2, 'title' => 'Two'],
      2 => ['id' => 3, 'title' => 'Three'],
      3 => ['id' => 4, 'title' => 'Three'],
      4 => ['id' => 5, 'title' => FALSE],
    ];

    // Non-strict + first
    $this->assertSame(1,     ArrayHelper::searchInTwodimArray($array, 'title', 'Two',        FALSE, 'first'));
    $this->assertSame(2,     ArrayHelper::searchInTwodimArray($array, 'title', 'Three',      FALSE, 'first'));
    $this->assertSame(0,     ArrayHelper::searchInTwodimArray($array, 'id',     1,           FALSE, 'first'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title',  FALSE,       FALSE, 'first'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title',  0,           FALSE, 'first'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title',  '0',         FALSE, 'first'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 'Non exists', FALSE, 'first'));

    // Strict + first
    $this->assertSame(1,     ArrayHelper::searchInTwodimArray($array, 'title', 'Two',        TRUE, 'first'));
    $this->assertSame(2,     ArrayHelper::searchInTwodimArray($array, 'title', 'Three',      TRUE, 'first'));
    $this->assertSame(0,     ArrayHelper::searchInTwodimArray($array, 'id',    1,            TRUE, 'first'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title', FALSE,        TRUE, 'first'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 0,            TRUE, 'first'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', '0',          TRUE, 'first'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 'Non exists', TRUE, 'first'));

    // Non-strict + last
    $this->assertSame(1,     ArrayHelper::searchInTwodimArray($array, 'title', 'Two',        FALSE, 'last'));
    $this->assertSame(3,     ArrayHelper::searchInTwodimArray($array, 'title', 'Three',      FALSE, 'last'));
    $this->assertSame(0,     ArrayHelper::searchInTwodimArray($array, 'id',    1,            FALSE, 'last'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title', FALSE,        FALSE, 'last'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title', 0,            FALSE, 'last'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title', '0',          FALSE, 'last'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 'Non exists', FALSE, 'last'));

    // Strict + last
    $this->assertSame(1,     ArrayHelper::searchInTwodimArray($array, 'title', 'Two',        TRUE, 'last'));
    $this->assertSame(3,     ArrayHelper::searchInTwodimArray($array, 'title', 'Three',      TRUE, 'last'));
    $this->assertSame(0,     ArrayHelper::searchInTwodimArray($array, 'id',    1,            TRUE, 'last'));
    $this->assertSame(4,     ArrayHelper::searchInTwodimArray($array, 'title', FALSE,        TRUE, 'last'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 0,            TRUE, 'last'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', '0',          TRUE, 'last'));
    $this->assertSame(FALSE, ArrayHelper::searchInTwodimArray($array, 'title', 'Non exists', TRUE, 'last'));

    // Non-strict + all
    $this->assertSame([1],    ArrayHelper::searchInTwodimArray($array, 'title', 'Two',        FALSE, 'all'));
    $this->assertSame([2, 3], ArrayHelper::searchInTwodimArray($array, 'title', 'Three',      FALSE, 'all'));
    $this->assertSame([0],    ArrayHelper::searchInTwodimArray($array, 'id',    1,            FALSE, 'all'));
    $this->assertSame([4],    ArrayHelper::searchInTwodimArray($array, 'title', FALSE,        FALSE, 'all'));
    $this->assertSame([4],    ArrayHelper::searchInTwodimArray($array, 'title', 0,            FALSE, 'all'));
    $this->assertSame([4],    ArrayHelper::searchInTwodimArray($array, 'title', '0',          FALSE, 'all'));
    $this->assertSame(FALSE,  ArrayHelper::searchInTwodimArray($array, 'title', 'Non exists', FALSE, 'all'));

    // Strict + all
    $this->assertSame([1],    ArrayHelper::searchInTwodimArray($array, 'title', 'Two',         TRUE, 'all'));
    $this->assertSame([2, 3], ArrayHelper::searchInTwodimArray($array, 'title', 'Three',       TRUE, 'all'));
    $this->assertSame([0],    ArrayHelper::searchInTwodimArray($array, 'id',     1,            TRUE, 'all'));
    $this->assertSame([4],    ArrayHelper::searchInTwodimArray($array, 'title',  FALSE,        TRUE, 'all'));
    $this->assertSame(FALSE,  ArrayHelper::searchInTwodimArray($array, 'title',  0,            TRUE, 'all'));
    $this->assertSame(FALSE,  ArrayHelper::searchInTwodimArray($array, 'title',  '0',          TRUE, 'all'));
    $this->assertSame(FALSE,  ArrayHelper::searchInTwodimArray($array, 'title',  'Non exists', TRUE, 'all'));
  }

  /**
   * @covers ::getFirstKey
   */
  public function testGetFirstKey(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
    ];
    $this->assertSame('foo', ArrayHelper::getFirstKey($array));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
    ];
    $this->assertSame(0, ArrayHelper::getFirstKey($array));
  }

  /**
   * @covers ::getLastKey
   */
  public function testGetLastKey(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
    ];
    $this->assertSame('bar', ArrayHelper::getLastKey($array));

    $array = [
      0 => 'Foo',
      1 => 'Bar',
    ];
    $this->assertSame(1, ArrayHelper::getLastKey($array));
  }

  /**
   * @covers ::sortBySecondArrayValues
   */
  public function testSortBySecondArrayValues(): void {
    $array = [
      'bar' => 'Bar',
      'baz' => 'Baz',
      'qwe' => 'Qwe',
      'foo' => 'Foo',
    ];
    $result = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
      'qwe' => 'Qwe',
    ];
    $this->assertSame($result, ArrayHelper::sortBySecondArrayValues($array, ['foo', 'bar', 'baz']));
  }

  /**
   * @covers ::sortBySecondArrayKeys
   */
  public function testSortBySecondArrayKeys(): void {
    $array = [
      'bar' => 'Bar',
      'baz' => 'Baz',
      'qwe' => 'Qwe',
      'foo' => 'Foo',
    ];
    $result = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
      'qwe' => 'Qwe',
    ];
    $this->assertSame($result, ArrayHelper::sortBySecondArrayKeys($array, ['foo' => '...', 'bar' => '...', 'baz' => '...']));
  }

  /**
   * @covers ::implodeMultiArray
   */
  public function testImplodeMultiArray(): void {
    $array = [
      ['one', 'two'],
      ['three', 'for'],
    ];
    $this->assertSame('one,two|three,for', ArrayHelper::implodeMultiArray($array, '|', ','));
  }

  /**
   * @covers ::formatArrayAsKeyValueList
   */
  public function testFormatArrayAsKeyValueList(): void {
    $array = [
      1 => 'Foo',
      2 => 'Bar',
    ];
    $this->assertSame("1: Foo\n2: Bar", ArrayHelper::formatArrayAsKeyValueList($array));
    $this->assertSame("1 = Foo<br />2 = Bar", ArrayHelper::formatArrayAsKeyValueList($array, ' = ', '<br />'));
  }

  /**
   * @covers ::formatKeyValueListAsArray
   */
  public function testFormatKeyValueListAsArray(): void {
    $this->assertSame(['1' => 'Foo', '2' => 'Bar'], ArrayHelper::formatKeyValueListAsArray("1:Foo\n2:Bar", ':'));
  }

  /**
   * @covers ::keysExists
   */
  public function testKeysExists(): void {
    $array = [
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ];

    $this->assertSame(TRUE, ArrayHelper::keysExists($array, ['foo', 'baz']));
    $this->assertSame(FALSE, ArrayHelper::keysExists($array, ['foo', 'qwe']));
  }

  /**
   * @covers ::columnIsEmpty
   */
  public function testColumnIsEmpty(): void {
    $array = [
      ['id' => 1, 'title' => 'One'],
      ['id' => 2, 'title' => ''],
    ];
    $this->assertSame(FALSE, ArrayHelper::columnIsEmpty($array, 'id'));
    $this->assertSame(FALSE, ArrayHelper::columnIsEmpty($array, 'title'));

    $array = [
      ['id' => 1, 'title' => ''],
      ['id' => 2, 'title' => ''],
    ];
    $this->assertSame(FALSE, ArrayHelper::columnIsEmpty($array, 'id'));
    $this->assertSame(TRUE, ArrayHelper::columnIsEmpty($array, 'title'));
  }

  /**
   * @covers ::getValueByIndex
   */
  public function testGetElementByIndex(): void {
    $array = [
      'foo' => 'Foo item',
      'bar' => 'Bar item',
      'baz' => 'Baz item',
    ];
    $this->assertSame($array['foo'], ArrayHelper::getValueByIndex($array, 0));
    $this->assertSame($array['bar'], ArrayHelper::getValueByIndex($array, 1));
    $this->assertSame($array['baz'], ArrayHelper::getValueByIndex($array, 2));
    $this->assertSame(NULL, ArrayHelper::getValueByIndex($array, 3));
  }

  /**
   * @covers ::arrayContainsAtLeastOneValueFromOtherArray
   */
  public function testArrayContainsAtLeastOneValueFromOtherArray(): void {
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['bar', 'baz']));
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['baz', 'bar']));
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['baz', 'bar'], ['foo', 'bar']));
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['foo', 'bar']));
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['bar']));
    $this->assertSame(TRUE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['bar'], ['foo', 'bar']));
    $this->assertSame(FALSE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo', 'bar'], ['baz', 'qwe']));
    $this->assertSame(FALSE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['baz', 'qwe'], ['foo', 'bar']));
    $this->assertSame(FALSE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['baz', 'qwe'], ['foo']));
    $this->assertSame(FALSE, ArrayHelper::arrayContainsAtLeastOneValueFromOtherArray(['foo'], ['baz', 'qwe']));
  }

  /**
   * @covers ::csvToArray
   */
  public function testCsvToArray(): void {
    $this->assertSame([
      0 => [
        'col_name1' => 'foo',
        'col_name2' => 'bar',
      ],
      1 => [
        'col_name1' => 'Hello World',
        'col_name2' => 'baz',
      ],
    ], iterator_to_array(ArrayHelper::csvToArray(__DIR__ . '/../../modules/druhels_test/files/csv_with_header.csv')));

    $this->assertSame([
      0 => [
        0 => 'foo',
        1 => 'bar',
      ],
      1 => [
        0 => 'Hello World',
        1 => 'baz',
      ],
    ], iterator_to_array(ArrayHelper::csvToArray(__DIR__ . '/../../modules/druhels_test/files/csv_without_header.csv', ';', FALSE)));
  }

}
