<?php

namespace Drupal\Tests\druhels\Unit;

use Drupal\druhels\FormHelper;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Drupal\druhels\FormHelper
 */
class FormHelperTest extends TestCase {

  /**
   * @covers ::cleanGetForm
   */
  public function testCleanGetForm(): void {
    $form = [
      'string' => [
        '#type' => 'textfield',
        '#title' => 'String',
      ],
      'form_id' => [],
      'form_build_id' => [],
      'form_token' => [],
    ];
    $form = FormHelper::cleanGetForm($form);
    $this->assertArrayNotHasKey('form_id', $form);
    $this->assertArrayNotHasKey('form_build_id', $form);
    $this->assertArrayNotHasKey('form_token', $form);
    $this->assertArrayHasKey('string', $form);
  }

  /**
   * @covers ::moveLabelsToPlaceholder
   */
  public function testMoveLabelsToPlaceholder(): void {
    $form = [
      'string' => [
        '#type' => 'textfield',
        '#title' => 'String',
      ],
      'required_string' => [
        '#type' => 'textfield',
        '#title' => 'Required string',
        '#required' => TRUE,
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => 'Submit',
        ],
      ],
    ];
    FormHelper::moveLabelsToPlaceholder($form);
    $this->assertSame('String', $form['string']['#placeholder']);
    $this->assertSame('Required string *', $form['required_string']['#placeholder']);
    $this->assertArrayNotHasKey('#placeholder', $form['actions']);
  }

}
