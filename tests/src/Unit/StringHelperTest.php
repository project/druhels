<?php

namespace Drupal\Tests\druhels\Unit;

use Drupal\druhels\StringHelper;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Drupal\druhels\StringHelper
 */
class StringHelperTest extends TestCase {

  /**
   * @covers ::removeDoubleSpaces
   */
  public function testRemoveDoubleSpaces(): void {
    $this->assertSame('foo bar', StringHelper::removeDoubleSpaces('foo bar'));
    $this->assertSame('foo bar', StringHelper::removeDoubleSpaces('foo  bar'));
    $this->assertSame('foo bar', StringHelper::removeDoubleSpaces('foo    bar'));
    $this->assertSame('foo bar baz', StringHelper::removeDoubleSpaces('foo    bar  baz'));
    $this->assertSame('foo bar ', StringHelper::removeDoubleSpaces('foo bar '));
  }

  /**
   * @covers ::pregMatch
   */
  public function testPregMatch(): void {
    $string = 'Hello, World!';
    $this->assertSame([0 => 'Hello,', 1 => 'Hello'], StringHelper::pregMatch('/^(.+),/', $string));
    $this->assertSame('Hello', StringHelper::pregMatch('/^(.+),/', $string, 1));
    $this->assertSame(NULL, StringHelper::pregMatch('/abc/', $string));
    $this->assertSame(NULL, StringHelper::pregMatch('/abc/', $string, 1));
  }

  /**
   * @covers ::convertToFloat
   */
  public function testConvertToFloat(): void {
    $this->assertSame(123.0, StringHelper::convertToFloat('123'));
    $this->assertSame(123.45, StringHelper::convertToFloat('123.45'));
    $this->assertSame(123.45, StringHelper::convertToFloat('123,45'));
    $this->assertSame(123456.78, StringHelper::convertToFloat('1234 56,78'));
    $this->assertSame(123.0, StringHelper::convertToFloat('123 RUB'));
  }

  /**
   * @covers ::removeHtmlComments
   */
  public function testRemoveHtmlComments(): void {
    $this->assertSame('Hello World', StringHelper::removeHtmlComments('Hello <!-- comment -->World'));
    $this->assertSame('<b>Hello World</b>', StringHelper::removeHtmlComments('<b>Hello <!-- comment -->World</b>'));
    $this->assertSame('<b>Hello</b> World', StringHelper::removeHtmlComments('<b>Hello</b> <!--comment1-->World<!--comment2-->'));
    $this->assertSame('', StringHelper::removeHtmlComments('<!-- Hello World -->'));
    $this->assertSame("Hello\n\nWorld", StringHelper::removeHtmlComments("Hello\n<!-- comment -->\nWorld"));
    $this->assertSame("Hello\n\nWorld", StringHelper::removeHtmlComments("Hello\n<!-- comment\ncomment -->\nWorld"));
    $this->assertSame('', StringHelper::removeHtmlComments("<!-- comment\ncomment -->"));
  }

}
