<?php

namespace Drupal\Tests\druhels\Unit;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\druhels\DateHelper;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\druhels\DateHelper
 */
class DateHelperTest extends KernelTestBase {

  /**
   * @covers ::createPhpDateTime
   */
  public function testCreatePhpDateTime(): void {
    $date_input = '2021-03-14 16:44:30 UTC';
    $date_format = 'd.m.Y, H:i:s';
    $date_output = '14.03.2021, 16:44:30';

    $this->assertSame($date_output, DateHelper::createPhpDateTime($date_input)->format($date_format));
    $this->assertSame($date_output, DateHelper::createPhpDateTime(strtotime($date_input))->format($date_format));
    $this->assertSame($date_output, DateHelper::createPhpDateTime(new DrupalDateTime($date_input))->format($date_format));
    $this->assertSame($date_output, DateHelper::createPhpDateTime(new \DateTime($date_input))->format($date_format));
    $this->assertSame(NULL, DateHelper::createPhpDateTime(NULL));
  }

  /**
   * @cover ::getDaysBetweenDates
   */
  public function testGetDaysBetweenDates(): void {
    $this->assertSame(13, DateHelper::getDaysBetweenDates('2021-03-01', '2021-03-14'));
    $this->assertSame(0, DateHelper::getDaysBetweenDates('2021-03-14', '2021-03-14'));
    $this->assertSame(1, DateHelper::getDaysBetweenDates('2021-03-14 00:00:00', '2021-03-15 06:00:00'));
    $this->assertSame(NULL, DateHelper::getDaysBetweenDates(NULL, '2021-03-14'));
    $this->assertSame(NULL, DateHelper::getDaysBetweenDates('2021-03-01', NULL));
  }

  /**
   * @coveres ::getDateTimestamp
   */
  public function testGetDateTimestamp(): void {
    $this->assertSame(1262304000, DateHelper::getDateTimestamp('2010-01-01 UTC'));
    $this->assertSame(1262304000, DateHelper::getDateTimestamp(strtotime('2010-01-01 UTC')));
    $this->assertSame(1262304000, DateHelper::getDateTimestamp(new \DateTime('2010-01-01 UTC')));
  }

  /**
   * @covers ::checkDateInDaterange
   */
  public function testCheckDateInDaterange(): void {
    $this->assertSame(TRUE, DateHelper::checkDateInDaterange('2010-01-01', '2009-12-30', '2010-02-01'));
    $this->assertSame(FALSE, DateHelper::checkDateInDaterange('2010-01-01', '2009-12-30', '2009-12-31'));
    $this->assertSame(TRUE, DateHelper::checkDateInDaterange('2010-01-01', '2009-12-30', NULL));
    $this->assertSame(TRUE, DateHelper::checkDateInDaterange('2010-01-01', NULL, '2010-02-01'));
    $this->assertSame(FALSE, DateHelper::checkDateInDaterange('2010-01-01', NULL, '2009-02-01'));
  }

  /**
   * @covers ::getDateSeason
   */
  public function testGetDateSeason(): void {
    $this->assertSame(0, DateHelper::getDateSeason('2010-01-10'));
    $this->assertSame(4, DateHelper::getDateSeason('2010-12-11'));
    $this->assertSame(1, DateHelper::getDateSeason('2011-03-02'));
    $this->assertSame(2, DateHelper::getDateSeason('2011-07-05'));
    $this->assertSame(3, DateHelper::getDateSeason('2015-10-16'));
  }

  /**
   * @covers ::getDateSeasonStart
   */
  public function testGetDateSeasonStart(): void {
    $this->assertSame('2009-12-01', DateHelper::getDateSeasonStart('2010-01-01'));
    $this->assertSame('2009-12-01', DateHelper::getDateSeasonStart('2010-01-02'));
    $this->assertSame('2009-12-01', DateHelper::getDateSeasonStart('2010-02-03'));
    $this->assertSame('2010-03-01', DateHelper::getDateSeasonStart('2010-03-04'));
    $this->assertSame('2010-06-01', DateHelper::getDateSeasonStart('2010-07-05'));
    $this->assertSame('2010-09-01', DateHelper::getDateSeasonStart('2010-10-06'));
    $this->assertSame('2010-12-01', DateHelper::getDateSeasonStart('2010-12-07'));
  }

  /**
   * @covers ::dateIsGreaterThan
   */
  public function testDateIsGreaterThan(): void {
    $this->assertSame(TRUE, DateHelper::dateIsGreaterThan('2020-01-10', '2010-01-01'));
    $this->assertSame(FALSE, DateHelper::dateIsGreaterThan('2010-01-01', '2020-01-10'));
    $this->assertSame(FALSE, DateHelper::dateIsGreaterThan('2010-01-01', '2010-01-01'));
  }

}
