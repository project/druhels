<?php

namespace Drupal\Tests\druhels\Unit;

use Drupal\druhels\FileHelper;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\druhels\FileHelper
 */
class FileHelperTest extends KernelTestBase {

  protected static $modules = [
    'file',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
  }

  /**
   * @covers ::getFileEntityByUri
   */
  public function testGetFileEntityByUri(): void {
    $file = File::create(['uri' => 'public://image.jpg']);
    $file->save();

    $loaded_file = FileHelper::getFileEntityByUri('public://image.jpg');
    $this->assertIsObject($loaded_file);
    $this->assertSame((int)$file->id(), (int)$loaded_file->id());
  }

  /**
   * @covers ::createFileEntityByUri
   */
  public function testCreateFileEntityByUri(): void {
    $file = FileHelper::createFileEntityByUri('public://image.jpg');
    $this->assertInstanceOf(FileInterface::class, $file);
    $this->assertSame('public://image.jpg', $file->getFileUri());
  }

  /**
   * @covers ::getFileExtension
   */
  public function testGetFileExtension(): void {
    $this->assertSame('jpg', FileHelper::getFileExtension('image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('/image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('/path/image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('public://image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('D:\image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('http://example.com/image.jpg'));
    $this->assertSame('jpg', FileHelper::getFileExtension('http://example.com/image.jpg?foo=bar'));
    $this->assertSame('htaccess', FileHelper::getFileExtension('.htaccess'));
    $this->assertSame('', FileHelper::getFileExtension(''));
  }

  /**
   * @covers ::getImageStyleUrl
   */
  public function testGetImageStyleUrl(): void {
    // @TODO
  }

  /**
   * @covers ::getFileStorage
   */
  public function testGetFileStorage(): void {
    $this->assertInstanceOf(FileStorageInterface::class, FileHelper::getFileStorage());
  }

}
