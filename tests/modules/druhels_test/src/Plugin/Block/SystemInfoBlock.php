<?php

namespace Drupal\druhels_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\UncacheableDependencyTrait;
use Drupal\druhels\DrupalHelper;
use Drupal\druhels\EntityHelper;
use Drupal\druhels\NodeHelper;
use Drupal\druhels\TaxonomyHelper;
use Drupal\druhels\UserHelper;

/**
 * @Block(
 *   id = "system_info_block",
 *   admin_label = @Translation("System info")
 * )
 */
class SystemInfoBlock extends BlockBase {

  use UncacheableDependencyTrait;

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $current_entity = EntityHelper::getCurrentEntity();
    $current_node = NodeHelper::getCurrentNode();
    $current_term = TaxonomyHelper::getCurrentTerm();

    return [
      'content' => [
        '#markup' => '
          Current title: ' . DrupalHelper::getCurrentPageTitle() . '<br />
          Current system path: ' . DrupalHelper::getCurrentSystemPath() . '<br />
          Current path alias: ' . DrupalHelper::getCurrentPathAlias() . '<br />
          Current route name: ' . DrupalHelper::getCurrentRouteName() . '<br />
          Is admin route: ' . (int)DrupalHelper::isAdminRoute() . '<br />
          Is front page: ' . (int)DrupalHelper::isFrontPage() . '<br />
          Default langcode: ' . DrupalHelper::getDefaultLangcode() . '<br />
          Current langcode: ' . DrupalHelper::getCurrentLangcode() . '<br />
          Is entity page: ' . (int)EntityHelper::isEntityPage() . '<br />
          Is entity node page: ' . (int)EntityHelper::isEntityPage('node') . '<br />
          Current entity id: ' . ($current_entity ? $current_entity->id() : '~') . '<br />
          Is node page: ' . (int)NodeHelper::isNodePage() . '<br />
          Is node "page" page: ' . (int)NodeHelper::isNodePage('page') . '<br />
          Current node id: ' . (NodeHelper::getCurrentNodeId() ?: '~') . '<br />
          Current node: ' . ($current_node ? $current_node->id() : '~') . '<br />
          Is term page: ' . (int)TaxonomyHelper::isTermPage() . '<br />
          Current term id: ' . (TaxonomyHelper::getCurrentTermId() ?: '~') . '<br />
          Current term: ' . ($current_term ? $current_term->id() : '~') . '<br />
          Current user has role administrator: ' . (int)UserHelper::currentUserHasRole('administrator') . '<br />
          Current user is admin: ' . (int)UserHelper::currentUserIsAdmin() . '<br />
        ',
      ],
    ];
  }

}
