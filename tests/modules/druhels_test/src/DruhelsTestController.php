<?php

namespace Drupal\druhels_test;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\druhels\DrupalHelper;
use Drupal\druhels\SeoHelper;

class DruhelsTestController extends ControllerBase {

  /**
   * Action for /druhels-test/change-page-title-in-runtime/{title}
   */
  public function changePageTitleInRuntime(string $title): array {
    DrupalHelper::setCurrentPageTitle($title, TRUE);

    return [
      '#markup' => 'ok',
    ];
  }

  /**
   * Action for /druhels-test/attach-html-head
   */
  public function attachHtmlHead(): array {
    $build = ['#markup' => 'ok'];
    SeoHelper::attachHtmlHead($build, 'test_link', 'link', ['rel' => 'test-link']);
    return $build;
  }

  /**
   * Action for /druhels-test/attach-metatag
   */
  public function attachMetatag(): array {
    $build = ['#markup' => 'ok'];
    SeoHelper::attachMetatag($build, 'test_metatag', 'test-metatag', 'foo bar baz');
    return $build;
  }

  /**
   * Action for /druhels-test/attach-jsonld
   */
  public function attachJsonld(): array {
    $build = ['#markup' => 'ok'];
    SeoHelper::attachJsonld($build, 'product_jsonld', [
      '@context' => 'https://schema.org/',
      '@type' => 'Product',
      'sku' => 'TESTPROD',
    ]);
    return $build;
  }

  /**
   * Action for /druhels-test/shemaorg-data
   */
  public function shemaorgData(): array {
    $build = [
      '#markup' => Markup::create(SeoHelper::renderSchemaorgData([
        'foo' => 'bar',
      ])),
    ];
    return $build;
  }

}
